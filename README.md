# Automated Root Phenotyping
Robotic's Master Thesis Project at CREATE-Lab. The focus of this project is to
extract root parameters in an automated fashion, by repurposing a cartesian
robot of an existing 3D printer and combining it with a camera.

## Run the code
To setup the system's software three **prerequisites must be fulfilled**:
 - The system requires **python**
 - The **Arduino IDE** must be installed to flash the control software for the peripherals
 - The GUI runs on any of the following **web-browsers**: Google Chrome, Firefox, Microsoft Edge

If those requisites are met, **proceed by**:
1. **flashing the Arduino** with the `Gripper.ino` project file
2. **Install the python dependencies** with the aid of the requirements.txt file  
2.1. `cd <path to project folder>/root-phenotyping/scripts`
2.2. `pip install -r requirements.txt`

Now, presuming that the hardware setup is ready to go, you can **launch the main app** script:
1. `cd <path to project folder>/root-phenotyping/scripts`
2. `python ./app.py`

Note that you might need to **adjust** the communication **parameters** for the peripherals, camera
and the Cartesian robot. The three main parameters are:
 -  `--peripherals-com`: The COM port for the peripherals, e.g. COM3
 -  `--robot-com`:  The COM port for the robot, e.g. COM4
 -  `--camera-idx`: The camera index used by open CV to interface with the camera, usually 0 or 1

The full list of prameters can be viewed with `python ./app.py -h`
As long as you don't close the terminal or exit the program via the GUI or keyboard interrupts
the program will run, even with closed GUI.

## Hardware Setup
The hardware setup is fairly easy (the assembly of the system will is not described here):
 - Connect the camera via USB to the central computer
 - Connect the arduino via USB to the central computer
 - Connect the Cartesian robot (ex 3D-printer) via USB to the central computer

Make sure the peripherals are connected correctly according to the following diagrams:

![Arduino Wiring Diagrams](arduino_connections.jpg?raw=true "Arduino Wiring Diagrams")

After powering up all components you are ready to run the software.
