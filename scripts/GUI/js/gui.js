eel.expose(require_GUI_update);
function require_GUI_update() {
  eel.updateGUI()((slot_data) => {
    var slots=document.getElementsByClassName('slot');

    for(var slot, i=0; slot=slots[i]; i++) {
      slot.id=slot_data[i];

      if(slot.id!="null") {
        slot.innerHTML=slot.id;
      } else {
        slot.innerHTML="";
      }
    }
  });
}

eel.expose(log_update);
function log_update() {
  //simply reload file
  document.getElementsByName("console_frame")[0].src=document.getElementsByName("console_frame")[0].src;
}

function set_day_time_cb() {
  var hh=parseInt(document.getElementsByName("day_h")[0].value);
  var mm=parseInt(document.getElementsByName("day_m")[0].value);

  eel.setDayTime(hh,mm);
}

function set_night_time_cb() {
  var hh=parseInt(document.getElementsByName("night_h")[0].value);
  var mm=parseInt(document.getElementsByName("night_m")[0].value);

  eel.setNightTime(hh,mm);

}

function set_rot_time_cb() {
  var hh=parseInt(document.getElementsByName("rot_h")[0].value);
  var mm=parseInt(document.getElementsByName("rot_m")[0].value);

  eel.setRotationIntervall(hh,mm);
}

function exit_cb() {
  if(confirm("Are you shure that you want to exit the programm")) {
    eel.exitProg();
  }
}

function pause_cb() {
  alert("Scheduling of tasks will continue, only use this for short pauses")
  eel.pauseSys();
}

function resume_cb() {
  if(confirm("Resume operation?")) {
    eel.resumeOperation()
  }
}

function home_cb() {
  if(confirm("!!WARNING!! Potential collision danger. Temporally remove the back right sample if the slot is occupied and lower the z stage, before continuing")) {
    eel.homeSys()(
      () => {
        alert("Click on \"Other Options -> Resume\" to resume operation");
      }
    );
  }
}

function lights_on_cb() {
  eel.lightsOnCb()
}

function lights_off_cb() {
  eel.lightsOffCb()
}

class RobotGUI {
  constructor(data_dir) {
    this.data_dir = data_dir;
    this.mode=0;
    this.init_GUI();
    this.close_new_sample_form();

    this.move_cb_couter=0;
    this.move_sample_id=undefined;
  }

  init_GUI() {
    eel.updateGUI()((slot_data) => {
      this.setup_GUI(slot_data);
      var slots=document.getElementsByClassName('slot');

      for(var s, i=0; s=slots[i]; i++) {
        s.addEventListener("click", this.generate_slot_click_cb(this));
      }
    });

    var mode_select = document.querySelectorAll('input[name="GUI_mode"]');

    for(var m, i=0; m=mode_select[i]; i++) {
      m.addEventListener("click", this.generate_mode_select_cb(this));
    }

    var add_sample_btn = document.querySelectorAll('button[name="add_sample"]')[0];
    add_sample_btn.addEventListener("click", this.add_new_sample_cb);

    document.getElementById("close_form_button").addEventListener("click", this.close_new_sample_form)

  }

  setup_GUI(slot_data) {
    var g=document.getElementById('gripper');
    var r=document.getElementById('rack');
    var g_slot = document.createElement('div');
    g_slot.id = slot_data[0];
    g_slot.dataset.slot_number=0;
    g_slot.className="slot";
    g.appendChild(g_slot);

    for(var i=1; i<slot_data.length; i++) {
      var r_slot = document.createElement('div');
      r_slot.id = slot_data[i];
      r_slot.dataset.slot_number=i;
      r_slot.className="slot";
      r_slot.id=slot_data[i];

      if(r_slot.id!="null") {
        r_slot.innerHTML=r_slot.id;
      }
      r.appendChild(r_slot);
    }
  }

  update_GUI() {
    eel.updateGUI()((slot_data) => {
      var slots=document.getElementsByClassName('slot');

      for(var slot, i=0; slot=slots[i]; i++) {
        slot.id=slot_data[i];

        if(slot.id!="null") {
          slot.innerHTML=slot.id;//"&#x1F331;";
        } else {
          slot.innerHTML="";
        }
      }
    });
  }

  generate_slot_click_cb(that) {
    //This function generates the callback function for the on-click event, its
    //definition is below:
    return function /*slot_click_cb*/() {
      //In the context of this function that refers to the RobotGUI object and
      //this to the element which generated the event
      //console.log(this);
      //console.log(that);

      switch (that.mode) {
        case "add":
          that.new_sample_cb(this);
          break;
        case "image":
          that.image_cb(this);
          break;
        case "move":
          that.move_cb(this);
          break;
        case "pickup":
          that.pickup_cb(this);
          break;
        case "put":
          that.put_cb(this);
          break;
        case "inspect":
        default:
          that.inspect_cb(this);
      }
    };
  }

  generate_mode_select_cb(that) {
    return function /*mode_select_cb*/() {
      that.mode = this.value;
    };
  }

  open_new_sample_form(target_slot) {
    document.forms["new_sample"]["target_slot"].value=target_slot;

    var form=document.getElementById('new_sample_form');
    document.getElementsByTagName("html")[0].style["overflow"]="hidden";
    form.style.display="flex";
  }

  close_new_sample_form() {
    var form=document.getElementById('new_sample_form');
    //clear form entries
    document.forms['new_sample']['target_slot'].value="";
    document.forms['new_sample']['sample_id'].value="";
    document.forms['new_sample']['sample_id'].style["border-color"]="";
    //document.forms['new_sample']['sample_name'].value="";
    document.forms['new_sample']['plant_count'].value="";
    form.style.display="none";
    document.getElementsByTagName("html")[0].style["overflow"]=""
  }

  require_confirmation(text,yes_cb,no_cb) {
    return true;
  }

  new_sample_cb(e) {
    var sample_id=e.id;
    //if the target slot is the gripper, do nothing
    if(e.dataset.slot_number==0)
      return;

    if(sample_id=="null" || confirm("Want to overwrite sample?")) {
      this.open_new_sample_form(e.dataset.slot_number);
    }
  }

  image_cb(e) {
    eel.imageSample(e.id)(() => {
      this.update_GUI()
    });
  }

  move_cb(e) {
    switch (this.move_cb_couter) {
      case 0:
        this.move_cb_couter=1;
        this.move_sample_id=e.id;
        e.style["border-color"]="#1090da";

        break;
      case 1:
        //do nothing if clicked on the same
        if(this.move_sample_id!=e.id) {

          //start pickup routine, define cb
          eel.pickupSample(this.move_sample_id)((ret) => {
            this.update_GUI()
            if(ret) {
              //start put routine, define cb
              eel.putSample(parseInt(e.dataset.slot_number)-1,true)((ret) => {
                this.update_GUI()
              });
            }
          });
        }
        //reset stuff
        this.move_cb_couter=0;
        document.getElementById(this.move_sample_id).style["border-color"]="";
        this.move_sample_id=undefined;
        e.style["border-color"]="";
        break;
      default:
        log("move_cb: should not be here, couter was different from 0 or 1");
        this.move_cb_couter=0;
    }
  }

  inspect_cb(e) {
    if(e.id!="null") {
      alert(e.id);
    }
    //load data from python? boh
  }

  add_new_sample_cb() {
    var form=document.getElementById('new_sample_form');
    var sample_id = document.forms['new_sample']['sample_id'];
    //var sample_name = document.forms['new_sample']['sample_name'];
    var plant_count = parseInt(document.forms['new_sample']['plant_count'].value);
    var target_slot = document.forms['new_sample']['target_slot'];
    var time_h = parseInt(document.forms['new_sample']['rec_int_h'].value)*3600;
    var time_m = parseInt(document.forms['new_sample']['rec_int_m'].value)*60;
    if(isNaN(time_h)) {
      time_h=24;
    }
    if(isNaN(time_m)) {
      time_m=0;
    }
    if(isNaN(plant_count)) {
      plant_count=0;
    }
    var rec_int_s = time_h+time_m; //in seconds
    console.log("recording sample every "+rec_int_s+" seconds");
    //check if form is correctly fileld in, id is required!
    if (sample_id.value=="") {
      var d=new Date();
      sample_id.value=d.toISOString().replaceAll(":","-").replaceAll(".","-");
      sample_id.style["border-color"]="orange";
      return;
    }

    //extract actual value once valid
    var sample_id_val=sample_id.value;
    //add new sample
    eel.addSample(sample_id_val,parseInt(target_slot.value)-1,rec_int_s,plant_count)((ret) => {
      eel.updateGUI()((slot_data) => {
        var slots=document.getElementsByClassName('slot');

        for(var slot, i=0; slot=slots[i]; i++) {
          slot.id=slot_data[i];

          if(slot.id!="null") {
            slot.innerHTML="&#x1F331;";
          } else {
            slot.innerHTML="";
          }
        }
      });
    });

    //reset form
    sample_id.value="";
    sample_id.style["border-color"]="";
    //sample_name.value="";
    plant_count.value="";
    document.forms['new_sample']['rec_int_h'].value="";
    document.forms['new_sample']['rec_int_m'].value="";

    //close form
    form.style.display="none";
    document.getElementsByTagName("html")[0].style["overflow"]="";
  }

  pickup_cb(e) {
    eel.pickupSample(e.id)((ret) => {
      this.update_GUI()
    });
  }

  put_cb(e) {
    eel.putSample(parseInt(e.dataset.slot_number)-1)((ret) => {
      this.update_GUI()
    });
  }
}
