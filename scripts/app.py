import eel
import os
import skimage as sk
import numpy as np
import threading
import time
import cv2 as cv
import argparse
try:
    from scripts.centralCtrl import *
    from scripts.scheduler import *
    from scripts.logger import *
except:
    from centralCtrl import *
    from scheduler import *
    from logger import *

#Init eel
eel.init('GUI')

#setup arg parser
parser = argparse.ArgumentParser()
parser.add_argument("--peripherals-com", help="The COM port for the peripherals, e.g. COM3", default="COM3")
parser.add_argument("--peripherals-baud", help="The used baudrate for the peripherals' serial communication, usually 9600", type=int, default=9600)
parser.add_argument("--peripherals-name", help="The used identifier for the peripherals, by default \"Arduino Peripherals\"", default="Arduino Peripherals")
parser.add_argument("--robot-com", help="The COM port for the robot, e.g. COM4", default="COM4")
parser.add_argument("--robot-baud", help="The used baudrate for the robot's serial communication, set by default to 115200", type=int, default=115200)
parser.add_argument("--robot-to", help="The timeout [s] for the robot's serial communication, set by default to 1s", type=int, default=1)
parser.add_argument("--camera-idx", help="The camera index used by open CV to interface with the camera, usually 0 or 1", type=int, default=1)
parser.add_argument("--rack-rows", help="The number of rows in the rack used in the setup, default 2", type=int, default=2)
parser.add_argument("--rack-slots", help="The number of slots per row in the rack used in the setup, default 6", type=int, default=6)
args = parser.parse_args()

#Set component's communication params
# Camera settings
camera_idx = args.camera_idx
# Gripper settings
g_com_port = args.peripherals_com
g_baud = args.peripherals_baud
g_name = args.peripherals_name
# Robot
r_com_port = args.robot_com
r_baud = args.robot_baud
r_timeout = args.robot_to
#Rack
rows= args.rack_rows
slots= args.rack_slots
#Logger
path_to_log="GUI/"

#Instantiate components
logger=HTMLLogger(path_to_log,eel)
tracker = SampleTracker(rows*slots,logger)
controller = CentralCtrl(
    CameraCtrl(camera_idx),
    CartesianRobot(r_com_port, r_baud, r_timeout),
    Peripherals(g_com_port, g_baud, device_name=g_name),
    tracker,
    logger,
    eel
)
scheduler = Scheduler(controller,tracker,logger)

def delete_objects(scheduler=scheduler,controller=controller,tracker=tracker):
    scheduler.stopThread()
    del scheduler
    del controller
    del tracker

@eel.expose
def exitProg():
    str="Exiting programm"
    logger.info(str)
    print(str)
    delete_objects()
    exit()

@eel.expose
def homeSys():
    str="Homing robot"
    logger.info(str)
    print(str)
    scheduler.requireHoming()

@eel.expose
def resumeOperation():
    str="Operation resumed"
    logger.info(str)
    print(str)
    scheduler.resumeTaskExecution()

@eel.expose
def pauseSys():
    str="Operation paused"
    logger.info(str)
    print(str)
    scheduler.pauseTaskExecution()

@eel.expose
def updateGUI():
    slots=tracker.getTotalSlots()
    for i in range(len(slots)):
        if slots[i] is not None:
            slots[i]=slots[i].id

    return slots

@eel.expose
def setDayTime(hh,mm):
    str=f"Update day time from {hh:02d}:{mm:02d}"
    logger.info(str)
    print(str)
    return scheduler.setDayTimeStart(hh,mm)

@eel.expose
def setNightTime(hh,mm):
    str=f"Update night time from {hh:02d}:{mm:02d}"
    logger.info(str)
    print(str)
    return scheduler.setNightTimeStart(hh,mm)

@eel.expose
def setRotationIntervall(hh,mm):
    str=f"Update rotation intervall to {hh:02d}:{mm:02d}"
    logger.info(str)
    print(str)
    return scheduler.setRotationTimeIntervall(hh,mm)

@eel.expose
def imageSample(sample_id):
    scheduler.addTask(Task(Task.RECORD_SAMPLE,sample_id=sample_id, priority=Task.NORMAL_PRIORITY))
    return

@eel.expose
def lightsOnCb():
    scheduler.addTask(Task(Task.LIGHTS_ON, priority=Task.HIGH_PRIORITY))
    return

@eel.expose
def lightsOffCb():
    scheduler.addTask(Task(Task.LIGHTS_OFF, priority=Task.HIGH_PRIORITY))
    return

@eel.expose
def pickupSample(sample_id):
    scheduler.addTask(Task(Task.PICKUP_SAMPLE,sample_id=sample_id,priority=Task.NORMAL_PRIORITY))
    return

@eel.expose
def putSample(target_slot):
    scheduler.addTask(Task(Task.PUT_SAMPLE,to_slot=target_slot,priority=Task.NORMAL_PRIORITY))
    return

@eel.expose
def addSample(sample_id, target_slot, recording_time_intervall_s, plant_count):
    ret = tracker.addSample(
        Sample(sample_id,target_slot,recording_time_intervall_s,plant_count),
        target_slot,
        True
    )

    if ret:
        try:
            os.mkdir(os.path.join(controller.PATH_TO_DATA,"samples",sample_id))
            os.mkdir(os.path.join(controller.PATH_TO_DATA,"samples",sample_id,"RAW"))
            os.mkdir(os.path.join(controller.PATH_TO_DATA,"samples",sample_id,"PRE"))
        except Exception as e:
            str=f"Could not create sample directory properly:\n {e}"
            logger.info(str)
            print(str)
    return ret

def close_cb(route, websockets):
    """
    This callback is defined to avoid the programm to stop when the browser
    window is closed. This allows the programm to continue running. GUI can
    always be accesed at the localhost link
    """
    if not websockets:
        str='Window was closed'
        logger.info(str)
        #actually do nothing, we want the server to keep running!
        print(str)


#eel.start("index.html", mode="firefox", close_callback=close_callback)
if __name__ == '__main__':
    try:
        os.mkdir(os.path.join(controller.PATH_TO_DATA,"samples"))
    except:
        data_str=os.path.join(controller.PATH_TO_DATA,"samples")
        str=f"could not generate {data_str} dir"
        logger.info(str)
        print(str)
    #start scheduling algorithm
    t=threading.Thread(target=scheduler.run)
    t.start()
    #start GUI
    eel.start("index.html", mode="firefox", close_callback=close_cb)
