# %% Imports
import cv2
import os
import pickle
import time

try:
    from scripts.class_prototypes.cameraCtrlBase import *
except:
    from class_prototypes.cameraCtrlBase import *


# %% Class Definition
class CameraCtrl(CameraCtrlBase):
    """
    Basic class for controlling the camera, to be extended to a rosnode later
    on.
    """
    RES_X=1920
    RES_Y=1080

    def __init__(self, camera_index, output_dir="", calib_data_dir="data/calibration/"):
        super(CameraCtrl, self).__init__()

        self._cam = cv2.VideoCapture(
            camera_index,
            apiPreference=cv2.CAP_ANY,
            params=[cv2.CAP_PROP_FRAME_WIDTH, self.RES_X, cv2.CAP_PROP_FRAME_HEIGHT, self.RES_Y]
        )
        self._out_dir = output_dir
        self._frame = None
        self._calib_mtx = None
        self._calib_dist = None
        self._undist_mzx = None

        self.loadCalibration(calib_data_dir)
        if self._calib_mtx is not None and self._calib_dist is not None:
            self._undist_mtx, _ = cv2.getOptimalNewCameraMatrix(
                self._calib_mtx,
                self._calib_dist,
                (self.RES_X,self.RES_Y),
                0,
                (self.RES_X,self.RES_Y)
            )


    def loadCalibration(self,path_to_data):
        """
        Calibration data must be stored as follows:
        path_to_data/camera_calib_mtx.pickle
        path_to_data/camera_calib_dist.pickle
        """
        mtx_path=os.path.join(path_to_data,"camera_calib_mtx.pickle")
        dist_path=os.path.join(path_to_data,"camera_calib_dist.pickle")
        try:
            with open(mtx_path, "rb") as f:
                self._calib_mtx = pickle.load(f)
            with open(dist_path, "rb") as f:
                self._calib_dist = pickle.load(f)
        except Exception as ex:
            print("Unable to load data with following exception:\n", ex)

    def correctDistortion(self,img):
        if self._undist_mtx is not None:
            return cv2.undistort(img, self._calib_mtx, self._calib_dist, None, self._undist_mtx)

        return img

    def recordImage(self, img_name):
        #wait for exposure to adjust
        time.sleep(3)
        (grabbed, self._frame) = self._cam.read()

        img_path = os.path.join(self._out_dir, img_name)
        print(img_path)

        if grabbed:
            self._frame = self.correctDistortion(self._frame)
            cv2.imwrite(img_path,self._frame)
            return True
        else:
            return False
