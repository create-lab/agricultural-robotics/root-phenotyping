#Partially copied from: https://learnopencv.com/camera-calibration-using-opencv/
#!/usr/bin/env python
# %% md
## Note: change the path to the calibration imgs and save folders
# %%
path_to_calib_img='scripts/data/calibration/camera_calib_imgs/*.jpg'
path_to_test_img='scripts/data/calibration/test_image.jpg'
path_to_save_folder='scripts/data/calibration/'
# %%
import cv2
import numpy as np
import os
import glob
import pickle

# Defining the dimensions of checkerboard
CHECKERBOARD = (6,9)
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# Creating vector to store vectors of 3D points for each checkerboard image
objpoints = []
# Creating vector to store vectors of 2D points for each checkerboard image
imgpoints = []


# Defining the world coordinates for 3D points
objp = np.zeros((1, CHECKERBOARD[0] * CHECKERBOARD[1], 3), np.float32)
objp[0,:,:2] = np.mgrid[0:CHECKERBOARD[0], 0:CHECKERBOARD[1]].T.reshape(-1, 2)
prev_img_shape = None

# Extracting path of individual image stored in a given directory
images = glob.glob(path_to_calib_img)
for fname in images:
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # Find the chess board corners
    # If desired number of corners are found in the image then ret = true
    ret, corners = cv2.findChessboardCorners(gray, CHECKERBOARD, cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_FAST_CHECK + cv2.CALIB_CB_NORMALIZE_IMAGE)

    """
    If desired number of corner are detected,
    we refine the pixel coordinates and display
    them on the images of checker board
    """
    if ret == True:
        objpoints.append(objp)
        # refining pixel coordinates for given 2d points.
        corners2 = cv2.cornerSubPix(gray, corners, (11,11),(-1,-1), criteria)

        imgpoints.append(corners2)

        # Draw and display the corners
        img = cv2.drawChessboardCorners(img, CHECKERBOARD, corners2, ret)

    #cv2.imshow('img',img)
    #cv2.waitKey(0)

cv2.destroyAllWindows()
#test calibration with image
img=cv2.imread(path_to_test_img)
h,w = img.shape[:2]

"""
Performing camera calibration by
passing the value of known 3D points (objpoints)
and corresponding pixel coordinates of the
detected corners (imgpoints)
"""
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

print("Camera matrix : \n")
print(mtx)
print("dist : \n")
print(dist)
print("rvecs : \n")
print(rvecs)
print("tvecs : \n")
print(tvecs)

# %% save stuff for later usage
try:
    with open(os.path.join(path_to_save_folder,"camera_calib_mtx.pickle"), "wb") as f:
        pickle.dump(mtx, f, protocol=pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(path_to_save_folder,"camera_calib_dist.pickle"), "wb") as f:
        pickle.dump(dist, f, protocol=pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(path_to_save_folder,"camera_calib_rvecs.pickle"), "wb") as f:
        pickle.dump(rvecs, f, protocol=pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(path_to_save_folder,"camera_calib_tvecs.pickle"), "wb") as f:
        pickle.dump(tvecs, f, protocol=pickle.HIGHEST_PROTOCOL)
except Exception as ex:
    print("Unable to save data with following exception:\n", ex)

# Refining the camera matrix using parameters obtained by calibration
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w,h), 0, (w,h))

# Method 1 to undistort the image
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

# Method 2 to undistort the image
mapx,mapy=cv2.initUndistortRectifyMap(mtx,dist,None,newcameramtx,(w,h),5)

dst = cv2.remap(img,mapx,mapy,cv2.INTER_LINEAR)

# Displaying the undistorted image
cv2.imshow("undistorted image",dst)
cv2.waitKey(0)
cv2.imwrite(os.path.join(path_to_save_folder,"rectified_image.jpg",dst)
