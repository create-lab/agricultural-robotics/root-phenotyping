import os
import skimage as sk
import numpy as np
from time import sleep
from datetime import datetime
try:
    #manage kernel run in parent dir
    from scripts.robotCtrl import *
    from scripts.cameraCtrl import *
    from scripts.peripheralCtrl import *
    from scripts.sampleTracker import *
except:
    from robotCtrl import *
    from cameraCtrl import *
    from peripheralCtrl import *
    from sampleTracker import *

class CentralCtrl(object):
    """docstring for CentralCtrl."""
    ## Properties
    # Coordinates range
    X_RANGE = [0,260]
    Y_RANGE = [0,260]
    Z_RANGE = [0,260]
    # Idle position
    X_IDLE = 130.0
    Y_IDLE = 130.0
    Z_IDLE = 60.0
    # Imaging position
    X_IMAG = 220.0
    Y_IMAG = 155.0
    Z_IMAG = 180.0
    # Pickup parameters
    Z_PICKUP = 25.0
    Z_APPROACH = 40.0
    X_APPROACH = -18.0
    # Put parameters
    Z_PUT_APPROACH = 120
    Z_PUT_ADJUST = 80
    X_PUT_APPROACH = -15
    # Movement speed
    XY_SPEED = 15000 #TODO(julbrich) max speed TBD
    XY_SPEED_LOAD = 8000
    Z_SPEED = 2400 #TODO(julbrich) max speed TBD
    # Rack
    X_SLOT_OFFSET = 50
    Y_ROW_OFFSET = 75
    X_SLOT_SPACING = 40.982
    Y_ROW_SPACING = 140
    ROW_COUNT = 2
    SLOT_COUNT = 6
    # Path to data folder
    PATH_TO_DATA = "data/"
    # Automatically launch the CV thread/process
    AUTO_CV = False

    def __init__(self,camera,robot,peripherals,sample_tracker,logger,gui):
        super(CentralCtrl, self).__init__()
        #instantiate components
        self._camera = camera
        self._robot = robot
        self._peripherals = peripherals
        self._tracker = sample_tracker
        self._logger=logger
        self._gui = gui

        self.xy_speed = self.XY_SPEED
        self.inDayCycle=False

        try:
            self._tracker.loadData(os.path.join(self.PATH_TO_DATA,"rack_data.pickle"))
        except Exception as e:
            str="could not load the rack data, samples must be added/loaded manually"
            if self._logger is not None: self._logger.error(str)
            print(str)

        #setup lookup for slot coordinates
        self._slot_coords = []

        for r in range(self.ROW_COUNT):
            for s in range(self.SLOT_COUNT):
                y = self.Y_ROW_OFFSET + r * self.Y_ROW_SPACING
                x = self.X_SLOT_OFFSET + s * self.X_SLOT_SPACING

                self._slot_coords.append((x,y))


    def __del__(self):

        if self._tracker.holdingSample():
            sample = self._tracker.getHoldingSample()
            ret = self.putSample(sample.home_slot)

        self._moveZToIdle()
        self._robot.moveXY(260,260,5000)
        self._waitForEndOfMovement()
        self._tracker.saveData()
        print("deleting stuff....")
        del self._robot
        del self._camera
        del self._peripherals

    def _checkXYCoordinates(self, x, y):
        ret = False

        if x <= self.X_RANGE[1] and x >= self.X_RANGE[0]:
            ret = True

        if y <= self.Y_RANGE[1] and y >= self.Y_RANGE[0]:
            ret = True
        else:
            ret = False

        return ret

    def _checkZCoordinate(self, z):
        if z <= self.Z_RANGE[1] and z >= self.Z_RANGE[0]:
            return True

        return False


    def _moveZToIdle(self):
        self._robot.moveZ(self.Z_IDLE)

    def _moveZToPickupHeight(self):
        self._robot.moveZ(self.Z_PICKUP)

    def _moveZToImagingHeight(self):
        self._robot.moveZ(self.Z_IMAG)

    def _moveGripperToPickupLocation(self,slot):
        """
        moves the gripper above the location of the requested sample
        @param row          the parameter of the row from which to grab the
                            sample
        @param slot         the slot of the give row to pick the sample from
        """

        x,y=self._getCoordinates(slot)

        if self._checkXYCoordinates(x,y):
            self._robot.moveXY(x,y, self.xy_speed)
        else:
            str="something went wrong with the conversion indices to coordinates"
            if self._logger is not None: self._logger.error(str)
            print(str)

    def _pickupApproachSample(self,slot):
        x,y=self._getCoordinates(slot)

        x = x + self.X_APPROACH
        #first approach in z dierction
        self._robot.moveZ(self.Z_APPROACH)
        #now correct x and z position simultaneously to rectify sample
        self._robot.move(x,y,self.Z_PICKUP)

    def _putApproachSample(self,slot):
        x,y=self._getCoordinates(slot)

        x = x + self.X_PUT_APPROACH

        self._robot.moveZ(self.Z_PUT_APPROACH)
        self._robot.move(x,y,self.Z_PUT_ADJUST)
        self._robot.moveZ(self.Z_PICKUP)

    def _moveGripperToImagingLocation(self):
        self._robot.moveXY(self.X_IMAG, self.Y_IMAG, self.xy_speed)

    def _waitForEndOfMovement(self):
        self._robot.readPosition()

    def _getCoordinates(self, slot):
        return self._slot_coords[slot]

    def _turnOnLEDs(self):
        return self._peripherals.turnOnLighting()

    def _turnOffLEDs(self):
        if self.inDayCycle:
            #don't turn off lights during day cycle
            return True

        return self._peripherals.turnOffLighting()

    def startDayCycle(self):
        self.inDayCycle=True
        return self._peripherals.turnOnLighting()

    def endDayCycle(self):
        self.inDayCycle=False
        return self._peripherals.turnOffLighting()

    # Public Methods
    def pickupSample(self, sample_id):
        if self._tracker.checkPickup(sample_id):
            #To be safe and avoid collisions move first to idle height which allows
            #the gripper to move freely above the samples, needs to be a blocking
            #movement. Also assure gripper is open
            self._moveZToIdle()
            self._peripherals.open_gripper()
            self._waitForEndOfMovement()
            if not self._peripherals.is_gripper_open():
                self._gui.require_GUI_update()
                return False

            slot=self._tracker.getSlotFromId(sample_id)
            #Pickup the sample
            self._moveGripperToPickupLocation(slot)
            self._pickupApproachSample(slot)
            self._waitForEndOfMovement()

            self._peripherals.close_gripper()
            if self._peripherals.is_gripper_open():
                self._gui.require_GUI_update()
                return False

            ret = self._tracker.trackPickip(sample_id)
            self._gui.require_GUI_update()

            self.xy_speed = self.XY_SPEED_LOAD

            #Assures that the gripper can move while holding the sample
            self._moveZToImagingHeight()
            self._waitForEndOfMovement()

            return ret
        elif self._tracker.holdingSample():
            sample = self._tracker.getHoldingSample()
            if sample.id == sample_id:
                #Already holding the right one
                self._gui.require_GUI_update()
                return True
            elif self._tracker.getSlotFromId(sample_id) is not None:
                #Only put down what it is holding if it can pickup something
                str=f"Putting back sample {sample.id}"
                if self._logger is not None: self._logger.info(str)
                print(str)
                ret = self.putSample(sample.home_slot)
                #If put sample fails pickup will not be attempted
                ret = ret and self.pickupSample(sample_id)
                self._gui.require_GUI_update()
                return ret

        self._gui.require_GUI_update()
        return False

    def imageSample(self,format=".jpg",auto_recording=False):
        """
        Does not check whether a sample has been picked up before, it blindly
        assumes it has been done.
        """

        self._moveGripperToImagingLocation()
        self._waitForEndOfMovement()

        sample = self._tracker.getHoldingSample()
        dir_name = os.path.join(self.PATH_TO_DATA,f"samples/{sample.id}/RAW/")
        img_name = dir_name + f"raw_{sample.img_count:04d}"
        img_name = img_name + format

        self._turnOnLEDs()
        #wait for exposure to correct
        sleep(1.5)
        ret = self._camera.recordImage(img_name)
        self._turnOffLEDs()

        if ret:
            sample.updateImgCount(img_name)
            if auto_recording:
                sample.updateRecordingTime()

            self._tracker.saveData()

        return ret

    def putSample(self, slot, position_overwrite=False):
        if self._tracker.checkPut(slot,position_overwrite):
            #To be safe and avoid collisions move first to imaging height which
            #allows the gripper to move freely above the samples while holding one,
            #needs to be a blocking movement.
            self._moveZToImagingHeight()
            self._waitForEndOfMovement()

            self._moveGripperToPickupLocation(slot)
            self._putApproachSample(slot)
            self._waitForEndOfMovement()

            self._peripherals.open_gripper()
            if not self._peripherals.is_gripper_open():
                self._gui.require_GUI_update()
                return False

            ret = self._tracker.trackPut(slot,position_overwrite)
            self._gui.require_GUI_update()

            self.xy_speed = self.XY_SPEED

            self._moveZToIdle()
            self._waitForEndOfMovement()

            return ret
        else:
            self._gui.require_GUI_update()
            return False

    def recordSample(self, sample_id):
        ret = self.pickupSample(sample_id)

        ret = ret and self.imageSample(auto_recording=True)

        return ret and self.putSample(self._tracker.getSampleHome(sample_id))

    def moveSample(self, sample_id, to):
        if self._tracker.slotIsLoaded(to):
            return False

        if self._tracker.getSlotFromId(sample_id)==to:
            return True

        ret = self.pickupSample(sample_id)

        return ret and self.putSample(to, position_overwrite=True)

    def homeRobot(self):
        self._robot.home()
        self._waitForEndOfMovement()
