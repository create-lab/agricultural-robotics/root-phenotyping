# %% Imports
import cv2
import os

# %% Class Definition
class CameraCtrlBase(object):
    """
    Basic class for controlling the camera, to be extended to a rosnode later
    on.
    """

    def __init__(self):
        super(CameraCtrlBase, self).__init__()

    def recordImage(self, img_name):
        raise NotImplementedError
