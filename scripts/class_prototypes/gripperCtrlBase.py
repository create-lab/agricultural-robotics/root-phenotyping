from time import sleep
try:
    #manage kernel run in parent dir
    from scripts.comms_wrapper import *
except:
    from comms_wrapper import *

class GripperCtrlBase(object):

    def __init__(self):
        super(GripperCtrlBase, self).__init__()

        self._is_open=False

    def reinit(self):
        raise NotImplementedError

    def open_gripper(self):
        raise NotImplementedError

    def close_gripper(self):
        raise NotImplementedError

    def is_gripper_open(self):
        return self._is_open
