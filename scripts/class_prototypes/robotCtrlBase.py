# %% Imports
import serial # import serial library
import imutils
from time import sleep

# %% Class Definition
class RobotCtrlBase(object):
    """docstring for PlatformCtrlBase."""

    def __init__(self):
        super(RobotCtrlBase, self).__init__()

    """
    The following routines are non-blocking and are queued by the robot.
    """
    def move(self, x, y, z, g_code = 0):
        raise NotImplementedError

    def home(self):
        raise NotImplementedError

    def moveX(self, x, speed=2400, g_code = 0):
        raise NotImplementedError

    def moveY(self, y, speed=2400, g_code = 0):
        raise NotImplementedError

    def moveXY(self, x, y, speed=2400, g_code = 0):
        raise NotImplementedError

    def moveZ(self, z, g_code = 0):
        raise NotImplementedError

    """
    Waits for the end of movement and is thus blocking
    """
    def readPosition(self):
        raise NotImplementedError
