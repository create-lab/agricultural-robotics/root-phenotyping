class SampleTrackerBase(object):
    """docstring for SampleTrackerBase."""

    def __init__(self):
        super(SampleTrackerBase, self).__init__()

    def addSample(self, sample, target_slot):
        raise NotImplementedError

    def initializeSlots(self,total_slots):
        raise NotImplementedError

    def slotIsLoaded(self, target_slot):
        raise NotImplementedError

    def holdingSample(self):
        raise NotImplementedError

    def checkPickup(self, sample):
        raise NotImplementedError

    def trackPickip(self, sample):
        raise NotImplementedError

    def checkPut(self, sample, position_overwrite=False):
        raise NotImplementedError

    def trackPut(self, sample, position_overwrite=False):
        raise NotImplementedError

    def getCurrentSample(self):
        raise NotImplementedError
