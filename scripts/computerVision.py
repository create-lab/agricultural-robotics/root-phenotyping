import cv2
import os
import glob
import pickle
from skimage import exposure, filters, morphology, measure
import time
try:
    from scripts.rootExtraction import *
    import scripts.util as util
    from scripts.reportGenerator import *
    from scripts.preprocessing import *
except:
    from rootExtraction import *
    import util as util
    from reportGenerator import *
    from preprocessing import *

class Classifier(object):
    """docstring for Classifier."""

    def __init__(self, preprocessor, logger, sample_tracker, report_generator=None,
                 path_to_data="data/"):
        super(Classifier, self).__init__()
        self.path_to_data = path_to_data
        self._tracker = sample_tracker
        self._preprocessor = preprocessor
        self._logger = logger
        self._report_generator=report_generator
        self._previous_roots=None

    def loadData(self,filename):
        try:
            with open(filename, "rb") as f:
                return pickle.load(f)
        except:
            return None

    def saveData(self,file_name,data):
        try:
            with open(file_name, "wb") as f:
                pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as ex:
            if self._logger is not None:
                self._logger.error(
                    f"[Classifier] Unable to save data with following exception:\n{ex}"
                )
            else:
                print("[Classifier] Unable to save data with following exception:\n", ex)

    def run_auto_analysis(self,sample_id):
        #get the sample
        sample = self._tracker.getSampleById(sample_id)
        #load image
        img=cv2.imread(sample.last_picture)

        bin,pre,roi,ovl=self._preprocessor(img)

        #check if old data is available
        self._previous_roots=self.loadData(
            os.path.join(self.path_to_data,"samples",sample.id,"previous_detection.pickle")
        )

        roots=self.classifyRoots(bin,self._previous_roots)
        self.generateReport(sample.id,roots,roi)

        self.saveData(
            os.path.join(self.path_to_data,"samples",sample.id,"previous_detection.pickle"),
            roots
        )

        #save detection story for data analysis
        detection_story=self.loadData(
            os.path.join(self.path_to_data,"samples",sample.id,"detection_story.pickle")
        )
        if detection_story is None:
            detection_story=[roots]
        else:
            detection_story.append(roots)
        self.saveData(
            os.path.join(self.path_to_data,"samples",sample.id,"detection_story.pickle"),
            detection_story
        )

    def classifyRoots(self,bin_img,previous_detection=None,root_shift_px_tolerance=10,
                      corr_thld=0.4,max_noise_len=0):
        roots=Root.findRoots(bin_img)

        final_roots=[]
        max_id=0
        if previous_detection is not None:
            max_id=len(previous_detection)
            final_roots=[None]*max_id

            ny,nx=bin_img.shape

            def drawRoot(chain,link,img):
                img[link.y,link.x]=1.0

            for prev_root in previous_detection:
                if not prev_root.is_root:
                    continue
                #1. Get nbh coords of root
                nbh_x_start=np.clip(prev_root.left_most_x-root_shift_px_tolerance,0,nx)
                nbh_x_end=np.clip(prev_root.right_most_x+root_shift_px_tolerance,0,nx)
                nbh_y_start=np.clip(prev_root.first.y-root_shift_px_tolerance,0,ny)
                nbh_y_end=np.clip(prev_root.deepest.y+root_shift_px_tolerance,0,ny)

                #2. Get detected roots that start in this nbh
                root_candidates=[]
                for root in roots:
                    if nbh_x_start <= root.first.x <= nbh_x_end and \
                       nbh_y_start <= root.first.y <= nbh_y_end:
                        if len(root_candidates)==0:
                            search_area_start_x=root.left_most_x
                            search_area_end_x=root.right_most_x
                            search_area_start_y=root.first.y
                            search_area_end_y=root.deepest.y
                        else:
                            if search_area_start_x>root.left_most_x:
                                search_area_start_x=root.left_most_x

                            if search_area_end_x<root.right_most_x:
                                search_area_end_x=root.right_most_x

                            if search_area_start_y>root.first.y:
                                search_area_start_y=root.first.y

                            if search_area_end_y<root.deepest.y:
                                search_area_end_y=root.deepest.y

                        root_candidates.append(root)

                #3. Compare current detection with previous one
                roots_to_concat=[]
                for root in root_candidates:
                    if root.total_length<max_noise_len:
                        continue
                    #3.1 Draw root on search map
                    search_map=np.zeros((ny,nx))
                    root.runAlongChain(root.first,drawRoot,search_map)

                    #3.2 Extract relevant nbh
                    nbh=search_map[
                     search_area_start_y:search_area_end_y+1,
                     search_area_start_x:search_area_end_x+1
                    ]

                    #3.3 Convolve with current root
                    root_kernel=root.generateRootKernel()
                    filtered=cv2.filter2D(nbh, -1, root_kernel)
                    h,w=filtered.shape

                    #3.4 Extract sub nbh to search max activation
                    sub_nbh_start_x=np.clip(
                        prev_root.first.x-search_area_start_x-root_shift_px_tolerance,0,w
                    )
                    sub_nbh_start_y=np.clip(
                        prev_root.first.y-search_area_start_y-root_shift_px_tolerance,0,h
                    )
                    d_sub_nbh=2*root_shift_px_tolerance+1
                    sub_nbh=np.array(
                        filtered[
                            sub_nbh_start_y:np.clip(sub_nbh_start_y+d_sub_nbh,0,h),
                            sub_nbh_start_x:np.clip(sub_nbh_start_x+d_sub_nbh,0,w)
                        ]
                    )

                    if sub_nbh.shape[0]==0 or sub_nbh.shape[1]==0:
                        #the root candidate is too far from actual root
                        continue
                    if np.max(sub_nbh) > corr_thld:
                        roots_to_concat.append(root)

                #4. Concatenate roots that need to be concatenated and save
                #   final roots
                if len(roots_to_concat)>1:
                    def drawShiftedRoot(chain,link,img,sx,sy):
                        if link.x-sx<0 or link.x-sx>=img.shape[1]:
                            return
                        if link.y-sy<0 or link.y-sy>=img.shape[0]:
                            return

                        img[link.y-sy,link.x-sx]=1.0

                    interpolation_map=np.zeros((ny,nx))
                    prev_root_kernel=prev_root.generateRootKernel()

                    for root in roots_to_concat:
                        roots.remove(root)
                        root.runAlongChain(root.first,drawRoot,interpolation_map)

                    filtered=cv2.filter2D(interpolation_map, -1, prev_root_kernel)
                    #idx=np.unravel_index(np.argmax(filtered),filtered.shape)

                    y_shift,x_shift=np.where(filtered>=corr_thld)
                    shift_v=np.array([prev_root.first.x-x_shift,prev_root.first.y-y_shift])
                    dist=np.linalg.norm(shift_v,axis=0)
                    if len(dist)>0:
                        idx_min_dist=np.argmin(dist)
                        x_shift=shift_v[0,idx_min_dist]
                        y_shift=shift_v[1,idx_min_dist]
                    else:
                        x_shift=0#prev_root.first.x-idx[1]
                        y_shift=0#prev_root.first.y-idx[0]


                    prev_root.runAlongChain(
                        prev_root.first,drawShiftedRoot,interpolation_map,x_shift,y_shift
                    )
                    interpolation_map=morphology.binary_dilation(interpolation_map)
                    interpolation_map=morphology.skeletonize(interpolation_map, method="lee")

                    new_roots=Root.findRoots(interpolation_map)

                    if len(new_roots)==1:
                        new_roots[0].root_classification_id=prev_root.root_classification_id
                        new_roots[0].is_root=True
                        final_roots[new_roots[0].root_classification_id]=new_roots[0]
                    elif len(new_roots)==0:
                        self._logger.error(
                            "[Classifier] Root interpolation failed and yielded no result during "
                            "root classification"
                        )
                        final_roots[prev_root.root_classification_id]=prev_root
                    else:
                        longest=None
                        length=0
                        for root in new_roots:
                            if root.total_length>length:
                                longest=root
                                length=root.total_length
                        longest.root_classification_id=prev_root.root_classification_id
                        longest.is_root=True
                        final_roots[longest.root_classification_id]=longest
                elif len(roots_to_concat)==1:
                    roots.remove(roots_to_concat[0])
                    roots_to_concat[0].root_classification_id=prev_root.root_classification_id
                    roots_to_concat[0].is_root=True
                    final_roots[roots_to_concat[0].root_classification_id]=roots_to_concat[0]
                else:
                    final_roots[prev_root.root_classification_id]=prev_root

        i=0
        for root in roots:
            if root.is_root:
                root.root_classification_id=max_id+i
                i=i+1
                final_roots.append(root)

        return final_roots

    def generateReport(self,sample_id,roots,img,raw_img_name,overlay=True,output_img=True,
                       draw_ramifications=True,draw_terminations=True,draw_deepest=True,
                       draw_start=True):
        def colorSegments(chain,link,root,img):
            if root.is_root:
                color_r=link.segment_id/root.number_segments
                img[link.y,link.x,0]=255*(link.segment_id%2)
                img[link.y,link.x,1]=(1-color_r)*255#255*(link.segment_id+1%2)#
                img[link.y,link.x,2]=color_r*255
        def overlayRoot(chain,link,img):
            img[link.y,link.x,0]=255
            img[link.y,link.x,1]=np.floor(img[link.y,link.x,1]/2)
            img[link.y,link.x,2]=np.floor(img[link.y,link.x,2]/2)

        if overlay:
            overlay_img=img.copy()
        else:
            overlay_img=None

        if output_img:
            output=np.zeros(img.shape)
        else:
            output=None

        root_idx=1
        root_str=[]
        for r in roots:
            if output_img and r.is_root:
                r.runAlongChain(r.first,colorSegments,r,output)

                #mark the starting point (red):
                dot_size=1
                if draw_start:
                    r_start=(r.first.x, r.first.y)
                    cv2.circle(output, r_start, dot_size, (0, 255, 0), -1)

                #mark terminations with expected growth direction (cyan):
                if draw_terminations:
                    line_length=8
                    for t in r.terminations:
                        cv2.line(
                            output, (t.x,t.y),
                            (t.x+int(line_length*np.sin(t.growth_direction)),
                            t.y+int(line_length*np.cos(t.growth_direction))),
                            (255, 255, 0),
                            1
                        )
                        cv2.circle(output, (t.x,t.y), dot_size, (255, 255, 0), -1)

                #mark depest (pink):
                if draw_deepest:
                    r_end=(r.deepest.x, r.deepest.y)
                    cv2.circle(output, r_end, dot_size, (255, 0, 255), -1)
                    #draw expected growth direction (pink line)
                    line_length=10
                    cv2.line(
                        output,
                        r_end,
                        (r_end[0]+int(line_length*np.sin(r.deepest.growth_direction)),
                        r_end[1]+int(line_length*np.cos(r.deepest.growth_direction))),
                        (255, 0, 255),
                        1
                    )

                #mark ramification nodes (yellow)
                if draw_ramifications:
                    r.findRamifications()
                    for rm in r.ramifications:
                        cv2.circle(output, (rm.x,rm.y), dot_size, (0, 255, 255), -1)


            if overlay and r.is_root:
                r.runAlongChain(r.first,overlayRoot,overlay_img)

            if r.is_root:
                root_str.append(r.printData(sample_id,root_idx))

            root_idx=root_idx+1


        cv2.imwrite(raw_img_name.replace("RAW\\raw","PRE\\overlay"), overlay_img)
        cv2.imwrite(raw_img_name.replace("RAW\\raw","PRE\\detection"), output)
        self._report_generator.addDataToReport(root_str)
        self._report_generator.convertToTxt()

        return output, overlay_img

    def manualBatchAnalysis(self,sample_id):
        images = glob.glob(os.path.join(self.path_to_data,"samples",sample_id,"RAW/*.jpg"))

        previous_detection=None
        detection_story=[]
        for fname in images:
            img = cv2.imread(fname)
            bin,pre,roi,ovl=self._preprocessor(img)
            previous_detection=self.classifyRoots(bin,previous_detection)
            detection_story.append(previous_detection)
            self.generateReport(sample_id,previous_detection,roi,fname)

        self.saveData(
            os.path.join(self.path_to_data,"samples",sample_id,"previous_detection.pickle"),
            previous_detection
        )
        self.saveData(
            os.path.join(self.path_to_data,"samples",sample_id,"detection_story.pickle"),
            detection_story
        )
