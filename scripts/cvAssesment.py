import cv2
import numpy as np
import glob
import os
import pickle
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns
import random
from scripts.computerVision import *
import scripts.util as util

# %% Set data retrival params
path_to_data="scripts/data/samples"
stored_plants_to_analyse_cv="analysis/plants_to_analyse.pickle"
stored_plants_to_analyse_gt="analysis/plants_to_analyse_gt.pickle"
wt=["col0","col1","col2"]
mut1=["brx0","brx1","brx2"]
mut2=["4brx0","4brx1","4brx2"]
save_data=False
load_data=True
save_gt_data=False
load_gt_data=True
px_to_mm=56/440

# %% Analyse
class RootAnalysis(object):
    """docstring for RootAnalysis."""

    def __init__(self, sample, plant_type, root_time_sequence, data_source="CV"):
        super(RootAnalysis, self).__init__()
        self.sample = sample
        self.plant_type=plant_type
        self.root_time_sequence=root_time_sequence
        self.root_length=[]
        self.root_depth=[]
        self.main_root_length=[]
        self.number_of_ramifications=[]
        self.length_of_sideshoots=[]
        self.data_source=data_source

    def plotData(self,figsize=(10,2)):
        fig=plt.figure(figsize=figsize)
        fig.suptitle(f"Experimental Data for Sample {self.sample} of Plant Type {self.plant_type}")
        gs = gridspec.GridSpec(1,5)

        plt.subplot(gs[0, 0])
        plt.title('Total Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.root_length)#,ax=axs[0])

        plt.subplot(gs[0, 1])
        plt.title('Main Root Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.main_root_length)#,ax=axs[1])

        plt.subplot(gs[0, 2])
        plt.title('Average Side-Shoot Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.length_of_sideshoots)#,ax=axs[2])

        plt.subplot(gs[0, 3])
        plt.title('Root Depth')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.root_depth)#,ax=axs[3])

        plt.subplot(gs[0, 4])
        plt.title('Number of Ramifications')
        plt.ylabel("count")
        plt.xlabel("[days]")
        sns.lineplot(data=self.number_of_ramifications)#,ax=axs[4])
        fig.tight_layout()

class Plotter(object):
    """docstring for Plotter."""

    def __init__(self, *plants):
        super(Plotter, self).__init__()
        self.plants=[*plants]
        self.labels=[]
        self.tot_len=[]
        self.main_len=[]
        self.side_len=[]
        self.depth=[]
        self.ram=[]
        for p in self.plants:
            self.labels.append(f"{p.sample} - {p.data_source}")
            self.tot_len.append(p.root_length)
            self.main_len.append(p.main_root_length)
            self.side_len.append(p.length_of_sideshoots)
            self.depth.append(p.root_depth)
            self.ram.append(p.number_of_ramifications)

    def plotData(self,figsize=(20,7)):
        fig=plt.figure(figsize=figsize)
        fig.suptitle(
            "Comparison between Groundtruth and Computer Vision Data for a Plant of Type "
            f"{self.plants[0].plant_type}"
        )
        gs = gridspec.GridSpec(1,5)

        plt.subplot(gs[0, 0])
        plt.title('Total Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.tot_len)#,ax=axs[0])
        plt.legend(title="Sample", loc='upper left', labels=self.labels)

        plt.subplot(gs[0, 1])
        plt.title('Main Root Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.main_len)#,ax=axs[1])
        plt.legend(title="Sample", loc='upper left', labels=self.labels)

        plt.subplot(gs[0, 2])
        plt.title('Average Side-Shoot Length')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.side_len)#,ax=axs[2])
        plt.legend(title="Sample", loc='upper left', labels=self.labels)

        plt.subplot(gs[0, 3])
        plt.title('Root Depth')
        plt.ylabel("[px]")
        plt.xlabel("[days]")
        sns.lineplot(data=self.depth)#,ax=axs[3])
        plt.legend(title="Sample", loc='upper left', labels=self.labels)

        plt.subplot(gs[0, 4])
        plt.title('Number of Ramifications')
        plt.ylabel("count")
        plt.xlabel("[days]")
        sns.lineplot(data=self.ram)#,ax=axs[4])
        plt.legend(title="Sample", loc='upper left', labels=self.labels)
        fig.tight_layout()



samples_id={"wild-type": wt, "brx" : mut1, "4brx" : mut2}
if load_data:
    with open(stored_plants_to_analyse_cv, "rb") as f:
        plants_to_analyse = pickle.load(f)
else:

    plants_to_analyse=[]
    for plant_type in samples_id:

        for sample_id in samples_id[plant_type]:

            filename=os.path.join(path_to_data,sample_id,"detection_story.pickle")

            with open(filename, "rb") as f:
                data_history = pickle.load(f)

            nb_recordings=len(data_history)
            nb_roots=len(data_history[-1])

            for recording_data in data_history:
                if len(recording_data)<nb_roots:
                    diff=nb_roots-len(recording_data)
                    recording_data.extend([None]*diff)

            #pick two random roots per sample
            first_plant=random.randint(0,nb_roots-1)
            secnd_plant=random.randint(0,nb_roots-1)
            while first_plant==secnd_plant:
                secnd_plant=random.randint(0,nb_roots-1)

            first_sequence=[]
            secnd_sequence=[]
            for recording_data in data_history:
                first_sequence.append(recording_data[first_plant])
                secnd_sequence.append(recording_data[secnd_plant])

            plants_to_analyse.append(RootAnalysis(sample_id,plant_type,first_sequence))
            plants_to_analyse.append(RootAnalysis(sample_id,plant_type,secnd_sequence))

# %% extract data:
def extractPlantFeatures(plants_to_analyse):
    for plant in plants_to_analyse:
        plant.root_depth=[]
        plant.number_of_ramifications=[]
        plant.main_root_length=[]
        plant.root_length=[]
        plant.length_of_sideshoots=[]
        for root_at_given_time in plant.root_time_sequence:
            if root_at_given_time is not None:
                plant.root_depth.append(root_at_given_time.root_depth*px_to_mm)
                plant.number_of_ramifications.append(len(root_at_given_time.terminations)-1)

                total_root_length=0
                side_shoot_length=0
                main_root_length=0
                nb_side_shoots=0
                for segment in root_at_given_time.segments:
                    if segment.id in root_at_given_time.main_root_path:
                        main_root_length=main_root_length+segment.length
                    else:
                        nb_side_shoots=nb_side_shoots+1
                        side_shoot_length=side_shoot_length+segment.length

                    total_root_length=total_root_length+segment.length

                if nb_side_shoots>0:
                    side_shoot_length=side_shoot_length/nb_side_shoots
                plant.main_root_length.append(main_root_length*px_to_mm)
                plant.root_length.append(total_root_length*px_to_mm)
                plant.length_of_sideshoots.append(side_shoot_length*px_to_mm)
            else:
                plant.root_depth.append(np.nan)
                plant.number_of_ramifications.append(np.nan)
                plant.main_root_length.append(np.nan)
                plant.root_length.append(np.nan)
                plant.length_of_sideshoots.append(np.nan)

extractPlantFeatures(plants_to_analyse)

# %% Save extracted plants
if save_data:
    with open("analysis/plants_to_analyse.pickle", "wb") as f:
        pickle.dump(plants_to_analyse, f, protocol=pickle.HIGHEST_PROTOCOL)
else:
    print("Data save skipped")

# %% Print root start for the roots to be located by a human
for plant in plants_to_analyse:
    start=plant.root_time_sequence[-1].first
    print(
        f"Root starts at ({start.x},{start.y}) for sample {plant.sample} with "
        f"{len(plant.root_time_sequence)} time samples"
    )

#set data for gt
plants_per_sample={
    "col0": 2,
    "col1": 1,
    "col2": 2,
    "brx0": 2,
    "brx1": 1,
    "brx2": 2,
    "4brx0": 1,
    "4brx1": 2,
    "4brx2": 2
}
gt_plant_classifier={
    "col0": -140,
    "col1": 0,
    "col2": 215,
    "brx0": -604,
    "brx1": 0,
    "brx2": -417,
    "4brx0": 0,
    "4brx1": 245,
    "4brx2": 525
}

# %% Preprocess images to extract roi for GT tracing
if load_gt_data:
    with open(stored_plants_to_analyse_gt, "rb") as f:
        plants_gt = pickle.load(f)
else:
    plants_gt=[]
    for plant_type in samples_id:
        for sample_id in samples_id[plant_type]:
            temp_plants=[]
            for i in range(14):
                fname = os.path.join(path_to_data,sample_id,f"GT/{(13-i)}.png")

                img = cv2.imread(fname)
                refined=morphology.skeletonize(img[:,:,2]>100, method="lee")

                roots=Root.findRoots(refined)
                roots.sort(key=lambda r: r.first.x)

                if plants_per_sample[sample_id] > 1 and plants_per_sample[sample_id]>=len(roots):
                    if len(temp_plants)==0:
                        for _ in range(plants_per_sample[sample_id]):
                            temp_plants.append(
                                RootAnalysis(sample_id,plant_type,[None]*14,data_source="GT")
                            )

                    for root in roots:
                        if root.first.x > np.abs(gt_plant_classifier[sample_id]):
                            if gt_plant_classifier[sample_id] > 0:
                                place_plant_to_idx=1
                            else:
                                place_plant_to_idx=0
                        else:
                            if gt_plant_classifier[sample_id] > 0:
                                place_plant_to_idx=0
                            else:
                                place_plant_to_idx=1

                        temp_plants[place_plant_to_idx].root_time_sequence[13-i]=root
                elif plants_per_sample[sample_id]>=len(roots):
                    if len(temp_plants)==0:
                        temp_plants.append(
                            RootAnalysis(sample_id,plant_type,[None]*14,data_source="GT")
                        )

                    if len(roots)==0:
                        temp_plants[0].root_time_sequence[13-i]=None
                    else:
                        temp_plants[0].root_time_sequence[13-i]=roots[0]

            plants_gt.extend(temp_plants)
# %% Save
if save_gt_data:
    with open("analysis/plants_to_analyse_gt.pickle", "wb") as f:
        pickle.dump(plants_gt, f, protocol=pickle.HIGHEST_PROTOCOL)
else:
    print("Data save skipped")

# %% Check gt
for plant in plants_gt:
    start=plant.root_time_sequence[-1].first
    print(
        f"Root starts at ({start.x},{start.y}) for sample {plant.sample} with "
        f"{len(plant.root_time_sequence)} time samples"
    )

extractPlantFeatures(plants_gt)

# %% Plot data
for cv, gt in zip(plants_to_analyse,plants_gt):
    c=Plotter(cv,gt)
    c.plotData()
# %%
sample_name=["Wild-Type (col-0)", "Mutant (brx)", "Quadruple Mutant (4brx)"]
labels=[
    ["col0","col0","col1","col2","col2"],
    ["brx0","brx0","brx1","brx2","brx2"],
    ["4brx0","4brx1","4brx1","4brx2","4brx2"]
]
sns.set()
for i in range(3):

    vs_fig=plt.figure(figsize=(10,15))
    vs_fig.suptitle(f"Ground Truth V.S. Computer Vision Data for {sample_name[i]}")
    gs = gridspec.GridSpec(3,4)

    plants=[]
    gt_cv_tot_len=plt.subplot(gs[0, 0:2])
    plt.title(f"Total Length for {sample_name[i]}")
    plt.ylabel("computer vision [mm]")
    plt.xlabel("ground truth [mm]")


    gt_cv_main_len=plt.subplot(gs[0, 2:4])
    plt.title(f"Main Root Length for {sample_name[i]}")
    plt.ylabel("computer vision [mm]")
    plt.xlabel("ground truth [mm]")

    gt_cv_avg_side=plt.subplot(gs[1, 0:2])
    plt.title(f"Average Side-Shoot Length for {sample_name[i]}")
    plt.ylabel("computer vision [mm]")
    plt.xlabel("ground truth [mm]")

    gt_cv_depth=plt.subplot(gs[1, 2:4])
    plt.title(f"Root Depth for {sample_name[i]}")
    plt.ylabel("computer vision [mm]")
    plt.xlabel("ground truth [mm]")

    gt_cv_rami=plt.subplot(gs[2, 1:3])
    plt.title(f"Number of Ramifications for {sample_name[i]}")
    plt.ylabel("computer vision [count]")
    plt.xlabel("ground truth [count]")

    for j in range(5):
        idx=j+i*5

        cv=plants_to_analyse[idx]
        gt=plants_gt[idx]

        plants.append(cv)
        plants.append(gt)

        plt.axes(gt_cv_main_len)
        plt.scatter(gt.main_root_length,cv.main_root_length)
        plt.legend(title='Plants from sample', loc='upper left', labels=labels[i])
        gt_cv_main_len.axis('equal')

        plt.axes(gt_cv_tot_len)
        plt.scatter(gt.root_length,cv.root_length)
        plt.legend(title='Plants from sample', loc='upper left', labels=labels[i])
        gt_cv_tot_len.axis('equal')

        plt.axes(gt_cv_avg_side)
        plt.scatter(gt.length_of_sideshoots,cv.length_of_sideshoots)
        plt.legend(title='Plants from sample', loc='upper left', labels=labels[i])
        gt_cv_avg_side.axis('equal')

        plt.axes(gt_cv_depth)
        plt.scatter(gt.root_depth,cv.root_depth)
        plt.legend(title='Plants from sample', loc='upper left', labels=labels[i])
        gt_cv_depth.axis('equal')

        plt.axes(gt_cv_rami)
        plt.scatter(gt.number_of_ramifications,cv.number_of_ramifications)
        plt.legend(title='Plants from sample', loc='upper left', labels=labels[i])
        gt_cv_rami.axis('equal')

    plt.axes(gt_cv_main_len)
    ymin, ymax = gt_cv_main_len.get_ylim()
    xmin, xmax = gt_cv_main_len.get_xlim()
    max_lim=np.maximum(ymax,xmax)
    min_lim=np.minimum(ymin,xmin)
    plt.plot([min_lim,max_lim],[min_lim,max_lim],'k--')

    plt.axes(gt_cv_tot_len)
    ymin, ymax = gt_cv_tot_len.get_ylim()
    xmin, xmax = gt_cv_tot_len.get_xlim()
    max_lim=np.maximum(ymax,xmax)
    min_lim=np.minimum(ymin,xmin)
    plt.plot([min_lim,max_lim],[min_lim,max_lim],'k--')

    plt.axes(gt_cv_avg_side)
    ymin, ymax = gt_cv_avg_side.get_ylim()
    xmin, xmax = gt_cv_avg_side.get_xlim()
    max_lim=np.maximum(ymax,xmax)
    min_lim=np.minimum(ymin,xmin)
    plt.plot([min_lim,max_lim],[min_lim,max_lim],'k--')

    plt.axes(gt_cv_depth)
    ymin, ymax = gt_cv_depth.get_ylim()
    xmin, xmax = gt_cv_depth.get_xlim()
    max_lim=np.maximum(ymax,xmax)
    min_lim=np.minimum(ymin,xmin)
    plt.plot([min_lim,max_lim],[min_lim,max_lim],'k--')

    plt.axes(gt_cv_rami)
    ymin, ymax = gt_cv_rami.get_ylim()
    xmin, xmax = gt_cv_rami.get_xlim()
    max_lim=np.maximum(ymax,xmax)
    min_lim=np.minimum(ymin,xmin)
    plt.plot([min_lim,max_lim],[min_lim,max_lim],'k--')

    vs_fig.tight_layout()
    plt.savefig(f"analysis/CV_vs_GT_{sample_name[i]}")
    c=Plotter(*plants)
    c.plotData()
    plt.savefig(f"analysis/CV_GT_comparison_{sample_name[i]}")
