from lxml import etree as ET
import os
import datetime

class HTMLLogger(object):
    """docstring for HTMLLogger."""

    def __init__(self, path_to_log, gui, log_name='log.html'):
        super(HTMLLogger, self).__init__()
        self.path_to_log=path_to_log
        self.log_name=log_name
        self._gui=gui
        #check if log file is around
        try:
            data = open(os.path.join(self.path_to_log,self.log_name),'r').read()
            doc = ET.HTML(data)
        except:
            doc=None

        if doc is None:
            html=ET.Element("html", {"lang":"en", "dir":"ltr"})
            head=html.makeelement("head")
            head.append(head.makeelement("meta", {"charset":"utf-8"}))
            head.append(head.makeelement("link", {"rel":"stylesheet", "href":"css/log.css"}))
            html.append(head)
            body=html.makeelement("body")
            body.text=""
            html.append(body)

            self._writeHtmlFile(os.path.join(self.path_to_log,self.log_name), html)

    def _reformatMessage(self,str):
        return str.replace("\n","<br>")

    def _openHtmlFile(self, path_to_file):
        try:
            data = open(path_to_file,'r').read()
            doc = ET.HTML(data)
        except:
            doc=None

        return doc

    def _writeHtmlFile(self, path_to_file, html):
        with open(path_to_file, "w") as html_file:
            html_file.write(ET.tostring(html).decode("utf-8").replace("\n","<br>"))

    def _addLogElement(self,root,str,log_priority="info"):
        #create info element
        elem = root.makeelement("div", {"class" : log_priority})
        #create timestamp
        timestamp=elem.makeelement("div", {"class" : "timestamp"})
        timestamp.text=f"{datetime.datetime.now()}"
        #create message
        message=elem.makeelement("div", {"class" : "message"})
        message.text=str

        #add stuff
        elem.append(timestamp)
        elem.append(message)
        root.insert(0,elem)

    def _requireGUIupdate(self):
        self._gui.log_update()

    def info(self,str):
        #load HTML file
        doc=self._openHtmlFile(os.path.join(self.path_to_log,self.log_name))
        root=doc[1] #get body

        #create info element
        self._addLogElement(root,str)

        #save file
        self._writeHtmlFile(os.path.join(self.path_to_log,self.log_name), doc)
        self._requireGUIupdate()

    def warn(self,str):
        #load HTML file
        doc=self._openHtmlFile(os.path.join(self.path_to_log,self.log_name))
        root=doc[1] #get body

        #create warning element
        self._addLogElement(root,str,log_priority="warn")

        #save file
        self._writeHtmlFile(os.path.join(self.path_to_log,self.log_name), doc)
        self._requireGUIupdate()

    def error(self,str):
        #load HTML file
        doc=self._openHtmlFile(os.path.join(self.path_to_log,self.log_name))
        root=doc[1] #get body

        #create info element
        self._addLogElement(root,str,log_priority="error")

        #save file
        self._writeHtmlFile(os.path.join(self.path_to_log,self.log_name), doc)
        self._requireGUIupdate()

    def convertToTxt(self):
        doc=self._openHtmlFile(os.path.join(self.path_to_log,self.log_name))
        body=doc[1] #get body

        path_to_file=os.path.join(self.path_to_log,self.log_name.replace("html","txt"))

        try:
            os.remove(path_to_file)
        except:
            pass

        with open(path_to_file, "a") as file:
            for el in body:
                msg=ET.tostring(el[1]).decode("utf-8").replace("<br/>","\n")
                msg=msg.replace("<div class=\"message\">","").replace("</div>","")
                t_stamp=el[0].text
                file.write(f"\n---> {t_stamp}:\n{msg}")
