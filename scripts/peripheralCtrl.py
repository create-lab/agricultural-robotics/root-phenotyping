from time import sleep
try:
    #manage kernel run in parent dir
    from scripts.comms_wrapper import *
    from scripts.class_prototypes.gripperCtrlBase import *
except:
    from comms_wrapper import *
    from class_prototypes.gripperCtrlBase import *


class GripperCtrl(GripperCtrlBase):
    """docstring for GripperCtrl."""

    MIN_APERTURE = 14    #soft bounds in mm, hard bounds handled by arduino
    MAX_APERTURE = 25
    MOVEMENT_TIMEOUT = 1.5 #s

    def __init__(self, com_port, baudrate, device_name = "Gripper"):
        super(GripperCtrl, self).__init__()
        self._is_open=True
        self._current_aperture=self.MAX_APERTURE

        self._ard = Arduino(descriptiveDeviceName=device_name,
                           portName=com_port, baudrate=baudrate)
        self._ard.connect_and_handshake()

        self.close_gripper()
        self.open_gripper()

    def reinit(self):
        self._ard.disconnect_arduino()
        self._ard.connect_and_handshake()

    def __del__(self):
        self._ard.disconnect_arduino()

    def open_gripper(self):
        self._is_open=self._ard.send_message(f"APR:{self.MAX_APERTURE}")
        self._is_open=bool(self._is_open)

        if self._is_open:
            #only update the current aperture if succesfully opened
            self._current_aperture=self.MAX_APERTURE

        sleep(self.MOVEMENT_TIMEOUT)
        return self._is_open

    def close_gripper(self):
        self._is_open=self._ard.send_message(f"APR:{self.MIN_APERTURE}")
        self._is_open=not bool(self._is_open)
        self._current_aperture=self.MIN_APERTURE
        sleep(self.MOVEMENT_TIMEOUT)
        return not self._is_open

    def setAperture(self,aperture):
        #check input value
        if aperture > self.MAX_APERTURE:
            print(f"Required aperture {aperture}mm exceedes maximum allowed: {self.MAX_APERTURE}mm")
            aperture = self.MAX_APERTURE
            return False
        elif aperture < 0:
            print(f"Aperture must be positive")
            aperture = 0
            return False

        self._is_open=self._ard.send_message(f"APR:{aperture}")
        self._is_open=bool(self._is_open)

        #update state variables
        if self._is_open and aperture < self.MAX_APERTURE:
            # It is considered close as soon as it is not completely open
            self._is_open=not bool(self._is_open)
            self._current_aperture=aperture
            return not self._is_open
        elif self._is_open:
            self._is_open=bool(self._is_open)
            self._current_aperture=aperture

        return self._is_open


    def is_gripper_open(self):
        return self._is_open

    def getCurrenAperture(self):
        return self._current_aperture

class Peripherals(GripperCtrl):
    """docstring for Peripherals."""

    def __init__(self, com_port, baudrate, device_name = "Peripherals"):
        super(Peripherals, self).__init__(com_port, baudrate, device_name)
        self.lightsAreOn=False

        self.turnOnLighting()
        time.sleep(1)
        self.turnOffLighting()

    def turnOnLighting(self):
        self.lightsAreOn=self._ard.send_message(f"LED:1")
        self.lightsAreOn=bool(self.lightsAreOn)

        return self.lightsAreOn

    def turnOffLighting(self):
        self.lightsAreOn=self._ard.send_message(f"LED:0")
        self.lightsAreOn=not bool(self.lightsAreOn)

        return not self.lightsAreOn
