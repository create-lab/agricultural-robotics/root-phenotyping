import cv2
import numpy as np
import glob
import os
import pickle
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn as sns
import pandas as pd
import sys
from scripts.computerVision import *

print(sys.getrecursionlimit())

max_rec = 0x100000

# May segfault without this line. 0x100 is a guess at the size of each stack frame.
sys.setrecursionlimit(max_rec)

# %% Set data retrival params
path_to_data="scripts/data/samples"
px_to_mm=56/440
sample_prefix=["col","brx","4brx"]
labels=["col-0","brx","4brx"]
run_classification=False
# %% Analyse
sns.set()
samples_color={
"col":"#4CB9E6",
"brx":"#E6637A",
"4brx":"#59E699"
}
outliers={
    "4brx0": [13,14,15,16],
    "4brx1": [13,14],
    "4brx2": [0,14,15],
    "brx0": [0,3],
    "brx1": [1,0],
    "brx2": [1,2,11,12,14,15,16],
    "col0": [0, 2, 9, 10, 12, 15, 18, 19, 20, 22, 24, 25, 27, 28],
    "col1": [9,12,14,15],
    "col2": [8,12,13,14,15]
}

avg_fig=plt.figure(figsize=(10,17))
avg_fig.suptitle(f"Mean and Standard Deviation of Chosen Traits for each Plant Type")
gs = gridspec.GridSpec(3,4)

avg_tot_len=plt.subplot(gs[0, 0:2])
plt.title('Total Length')
plt.ylabel("[mm]")
plt.xlabel("[days]")

avg_main_len=plt.subplot(gs[0, 2:4])
plt.title('Main Root Length')
plt.ylabel("[mm]")
plt.xlabel("[days]")

avg_avg_side=plt.subplot(gs[1, 0:2])
plt.title('Average Side-Shoot Length')
plt.ylabel("[mm]")
plt.xlabel("[days]")

avg_depth=plt.subplot(gs[1, 2:4])
plt.title('Root Depth')
plt.ylabel("[mm]")
plt.xlabel("[days]")

avg_rami=plt.subplot(gs[2, 1:3])
plt.title('Number of Ramifications')
plt.ylabel("count")
plt.xlabel("[days]")

for sample_pref in sample_prefix:
    roots_to_analyse=[]
    root_depth=[]
    number_of_ramifications=[]
    main_root_length=[]
    root_length=[]
    length_of_sideshoots=[]
    for i in range(3):
        sample_id=f"{sample_pref}{i}"
        if run_classification:
            rep=HTMLReportGenerator(f"scripts/data/samples/{sample_id}/", None)
            pre=Preprocrssor()
            c=Classifier(pre,None,None,report_generator=rep,path_to_data="scripts/data")
            c.manualBatchAnalysis(sample_id)

        filename=os.path.join(path_to_data,sample_id,"detection_story.pickle")

        data_history=None
        with open(filename, "rb") as f:
            data_history=pickle.load(f)

        nb_recordings=len(data_history)
        nb_roots=len(data_history[-1])

        for recording_data in data_history:
            if len(recording_data)<nb_roots:
                diff=nb_roots-len(recording_data)
                recording_data.extend([None]*diff)


        for r in range(nb_roots):
            if r in outliers[sample_id]:
                continue
            else:
                root_sequence = []
                root_depth_sequence=[]
                number_of_ramifications_sequence=[]
                main_root_length_sequence=[]
                root_length_sequence=[]
                length_of_sideshoots_sequence=[]
                for t in range(nb_recordings):
                    seq_start=1

                    root_at_given_time=data_history[t][r]

                    if root_at_given_time is not None and t>seq_start:
                        total_root_length=0
                        side_shoot_length=0
                        root_main_length=0
                        nb_side_shoots=0
                        for segment in root_at_given_time.segments:
                            if segment.id in root_at_given_time.main_root_path:
                                root_main_length=root_main_length+segment.length
                            else:
                                nb_side_shoots=nb_side_shoots+1
                                side_shoot_length=side_shoot_length+segment.length

                            total_root_length=total_root_length+segment.length

                        if nb_side_shoots>0:
                            side_shoot_length=side_shoot_length/nb_side_shoots

                        root_sequence.append(root_at_given_time)
                        root_depth_sequence.append(root_at_given_time.root_depth*px_to_mm)
                        number_of_ramifications_sequence.append(
                            len(root_at_given_time.terminations)-1
                        )
                        main_root_length_sequence.append(root_main_length*px_to_mm)
                        root_length_sequence.append(total_root_length*px_to_mm)
                        length_of_sideshoots_sequence.append(side_shoot_length*px_to_mm)
                    else:
                        root_sequence.append(root_at_given_time)
                        root_depth_sequence.append(np.nan)
                        number_of_ramifications_sequence.append(np.nan)
                        main_root_length_sequence.append(np.nan)
                        root_length_sequence.append(np.nan)
                        length_of_sideshoots_sequence.append(np.nan)

                roots_to_analyse.append(root_sequence)
                root_depth.append(root_depth_sequence)
                number_of_ramifications.append(number_of_ramifications_sequence)
                main_root_length.append(main_root_length_sequence)
                root_length.append(root_length_sequence)
                length_of_sideshoots.append(length_of_sideshoots_sequence)
                print(len(root_depth_sequence))


    x = np.arange(nb_recordings)

    root_depth_avg=np.nanmean(np.array(root_depth), axis=0)
    number_of_ramifications_avg=np.nanmean(np.array(number_of_ramifications), axis=0)
    main_root_length_avg=np.nanmean(np.array(main_root_length), axis=0)
    root_length_avg=np.nanmean(np.array(root_length), axis=0)
    length_of_sideshoots_avg=np.nanmean(np.array(length_of_sideshoots), axis=0)

    root_depth_std=np.nanstd(np.array(root_depth), axis=0)
    number_of_ramifications_std=np.nanstd(np.array(number_of_ramifications), axis=0)
    main_root_length_std=np.nanstd(np.array(main_root_length), axis=0)
    root_length_std=np.nanstd(np.array(root_length), axis=0)
    length_of_sideshoots_std=np.nanstd(np.array(length_of_sideshoots), axis=0)


    plt.axes(avg_main_len)
    plt.plot(x, main_root_length_avg, '-', color=samples_color[sample_pref])
    plt.fill_between(
        x,
        main_root_length_avg - main_root_length_std,
        main_root_length_avg + main_root_length_std,
        color=samples_color[sample_pref],
        alpha=0.1
    )
    plt.legend(title='Plants from sample', loc='upper left', labels=labels)
    #avg_main_len.set_aspect(3/4)

    plt.axes(avg_tot_len)
    plt.plot(x, root_length_avg, '-', color=samples_color[sample_pref])
    plt.fill_between(
        x,
        root_length_avg - root_length_std,
        root_length_avg + root_length_std,
        color=samples_color[sample_pref],
        alpha=0.1
    )
    plt.legend(title='Plants from sample', loc='upper left', labels=labels)
    #avg_tot_len.set_aspect(3/4)

    plt.axes(avg_avg_side)
    plt.plot(x, length_of_sideshoots_avg, '-', color=samples_color[sample_pref])
    plt.fill_between(
        x,
        length_of_sideshoots_avg - length_of_sideshoots_std,
        length_of_sideshoots_avg + length_of_sideshoots_std,
        color=samples_color[sample_pref],
        alpha=0.1
    )
    plt.legend(title='Plants from sample', loc='upper left', labels=labels)
    #avg_avg_side.set_aspect(3/4)

    plt.axes(avg_depth)
    plt.plot(x, root_depth_avg, '-', color=samples_color[sample_pref])
    plt.fill_between(
        x,
        root_depth_avg - root_depth_std,
        root_depth_avg + root_depth_std,
        color=samples_color[sample_pref],
        alpha=0.1
    )
    plt.legend(title='Plants from sample', loc='upper left', labels=labels)
    #avg_depth.set_aspect(3/4)

    plt.axes(avg_rami)
    plt.plot(x, number_of_ramifications_avg, '-', color=samples_color[sample_pref])
    plt.fill_between(
        x,
        number_of_ramifications_avg - number_of_ramifications_std,
        number_of_ramifications_avg + number_of_ramifications_std,
        color=samples_color[sample_pref],
        alpha=0.1
    )
    plt.legend(title='Plants from sample', loc='upper left', labels=labels)
    #avg_rami.set_aspect(3/4)

avg_fig.tight_layout()
plt.savefig(f"analysis/plant_data.png")
