import cv2
import numpy as np
import os
import glob
from skimage import exposure, filters, morphology, measure
try:
    import scripts.util as util
except:
    import util as util

################################################################################
### CLASS DEF ##################################################################
################################################################################
class ArUco(object):
    """docstring for ArUco."""

    def __init__(self, id, corners):
        """
        id and conrers from cv2.aruco.detectMarkers(bla)
        """
        super(ArUco, self).__init__()
        self.id = id
        x_coords, y_coords = corners.transpose()
        self.topL = (int(x_coords[0]),int(y_coords[0]))
        self.topR = (int(x_coords[1]),int(y_coords[1]))
        self.botR = (int(x_coords[2]),int(y_coords[2]))
        self.botL = (int(x_coords[3]),int(y_coords[3]))
        self.rearrangeCorners([self.botR,self.topR,self.botL,self.topL])
        self.center = self.computeCenter(corners)

    def rearrangeCorners(self,corners):
        """
        probably not the smartest way to do it, but least effort for my brain rn
        makes sure this is respected [topLeft,topRight,bottomRight,bottomLeft]
        """
        for i in range(len(corners)):
            x_i,y_i = corners[i]
            bt_count = 0;
            lr_count = 0;
            for j in range(len(corners)):
                x_j,y_j = corners[j]
                if x_i>x_j:
                    lr_count=lr_count+1
                if y_i>y_j:
                    bt_count=bt_count+1

            if lr_count < 2:
                #left
                if bt_count < 2:
                    #top
                    self.topL=(x_i,y_i)
                else:
                    self.botL=(x_i,y_i)
            else:
                if bt_count < 2:
                    self.topR=(x_i,y_i)
                else:
                    self.botR=(x_i,y_i)

    def computeCenter(self,corners):
        """
        this method works as long as the corners are set in (couter-)clockwise
        sens, i.e. sequentially as provided by cv2.aruco.detectMarkers,
        not randomly.

        (x0,y0) _______ (x1,y1)
                |     |
        (x3,y3) |_____| (x2,y2)
        """
        x,y=corners.transpose()

        ax=x[1]-x[0]
        bx=x[2]-x[0]
        cx=x[1]-x[3]

        ay=y[1]-y[0]
        by=y[2]-y[0]
        cy=y[1]-y[3]

        alpha=(ax-cx*(ay/cy))/(bx-cx*(by/cy))

        xC=x[0]+alpha*bx
        yC=y[0]+alpha*by

        return int(xC), int(yC)


class Preprocrssor(object):
    """docstring for Preprocrssor."""

    def __init__(self):
        super(Preprocrssor, self).__init__()
        self.arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_100)
        self.arucoParams = cv2.aruco.DetectorParameters_create()
        #TODO: define "proper const" in accordance with other files!!!!!!!!!!!!!
        self._ids=[0,13,42,69]
        self._sampleWidthPx=750
        self._sampleHeightPx=440
        self.kernels=[]
        self.kernel_weights=[]
        self.kernel_bounds=[]

        self._loadConvolutionKernels()

    def _loadConvolutionKernels(self):
        self.kernels=[]
        # TODO: save kernel weights as pickle and load them properly
        self.kernel_weights=np.array(
            [
                1.0,1.0,0.8,0.7,0.6,0.5,0.5,0.5,0.6,0.7,0.8,1.0,
                1.0,1.0,0.8,0.7,0.6,0.5,0.5,0.5,0.6,0.7,0.8,1.0
            ]
        )

        for i in range(24):
            img = cv2.imread(f"scripts/data/kernels/{i}.png")
            kernel=img[:,:,0]

            kernel=util.minmax(kernel)
            #square kernels assumed
            px_avg=np.sum(kernel)/(kernel.shape[0]**2)
            kernel=kernel-px_avg

            self.kernels.append(kernel)

    def getArUcoMarkers(self,img):
        (corners, ids, rej) = cv2.aruco.detectMarkers(
            img, self.arucoDict, parameters=self.arucoParams
        )

        if ids is None:
            raise Exception("No ArUco Markers found in image")
        ids = ids.reshape(len(ids))

        if len(ids) != len(self._ids):
            raise Exception(f"ArUco id count mismatch, {len(self._ids)} expected, got {len(ids)}")

        temp_ids1=ids.copy()
        temp_ids2=self._ids.copy()
        temp_ids1.sort()
        temp_ids2.sort()
        if not all(map(lambda x, y: x == y, temp_ids1, temp_ids2)):
            raise Exception(f"id mismatch between expected ids {self._ids} and got {ids}")

        markers = []
        for markerCorners, markerId in zip(corners, ids):
            markers.append(ArUco(markerId,markerCorners))

        return self.arrangeMarkersClockwise(markers)

    def arrangeMarkersClockwise(self,markers):
        """
        probably not the smartest way to do it, but least effort for my brain rn
        [topLeft,topRight,bottomRight,bottomLeft]
        """

        centers = []
        for m in markers:
            centers.append(m.center)

        markers_out=[None]*len(centers)
        for i in range(len(centers)):
            x_i,y_i = centers[i]
            bt_count = 0;
            lr_count = 0;
            for j in range(len(centers)):
                x_j,y_j = centers[j]
                if x_i>x_j:
                    lr_count=lr_count+1
                if y_i>y_j:
                    bt_count=bt_count+1

            if bt_count < 2:
                bt=0#top
            else:
                bt=2#bottom

            if lr_count < 2:
                lr=int(0+bt/2)#left
            else:
                lr=int(1-bt/2)#right

            markers_out[lr+bt]=markers[i]

        return markers_out

    def manualROIExtraction(self, img, ptsPreCorr):
        targetPts = np.float32(
            [
                (0,0),
                (self._sampleWidthPx,0),
                (self._sampleWidthPx,self._sampleHeightPx),
                (0,self._sampleHeightPx)
            ]
        )

        mtx = cv2.getPerspectiveTransform(ptsPreCorr,targetPts)
        return cv2.warpPerspective(img,mtx,(self._sampleWidthPx,self._sampleHeightPx))

    def extractROI(self,img):
        #mask lower part to avoid detecting the ArUcos of the samples in the rack
        img[1000:,:,:]=0

        markers = self.getArUcoMarkers(img)

        #the true ROI starts 2mm below the top markers and ends 3mm above the
        #lower markers. The marker side is 11mm, thus the 3/11. Here the vector
        #along the marker side is computed and then scaled to get to the true
        #border
        vector_scaling=(1+3/11)
        get_vector=lambda f,t: t-f
        scale_vector=lambda v: vector_scaling*v
        vector_addition=lambda v,w: int(v+w) #int cast since those will be array indices

        #top left ROI corner
        v=tuple(map(get_vector, markers[0].topL, markers[0].botL))
        v=tuple(map(scale_vector, v))
        topL_ROI=tuple(map(vector_addition, v, markers[0].topL))
        #top right ROI corner
        v=tuple(map(get_vector, markers[1].topR, markers[1].botR))
        v=tuple(map(scale_vector, v))
        topR_ROI=tuple(map(vector_addition, v, markers[1].topR))
        #bottom left ROI corner
        v=tuple(map(get_vector, markers[3].botL, markers[3].topL))
        v=tuple(map(scale_vector, v))
        botL_ROI=tuple(map(vector_addition, v, markers[3].botL))
        #bottom right ROI corner
        v=tuple(map(get_vector, markers[2].botR, markers[2].topR))
        v=tuple(map(scale_vector, v))
        botR_ROI=tuple(map(vector_addition, v, markers[2].botR))

        ptsPreCorr = np.float32([topL_ROI,topR_ROI,botR_ROI,botL_ROI])

        return self.manualROIExtraction(img, ptsPreCorr)

    def adjustContrast(self,img,avg_thld=1.2,contrast=30):
        """
        computes the average of the image and raisies pixels values above
        average and lowers those below
        contrast variable must be in range of ]0,inf]
        """
        avg=np.mean(img)
        diff=img-avg*avg_thld
        img[diff<0]=np.maximum(0.0,np.power(1+diff[diff<0],contrast))*img[diff<0]
        img[diff>0]=np.minimum(1.0,np.power(1+diff[diff>0],contrast))*img[diff>0]

        return img

    def enhanceFeatures(self,img):
        out=None
        for kernel, kernel_weight in zip(self.kernels, self.kernel_weights):
            filtered=cv2.filter2D(img, -1, kernel)

            # filtered=util.minmax(filtered)
            #
            # filtered=self.adjustContrast(filtered)
            if out is None:
                out=kernel_weight*filtered.copy()
            else:
                out=np.maximum(filtered*kernel_weight,out)

        out=util.minmax(out)

        out=self.adjustContrast(out)

        return util.minmax(out)

    def detectRoots(self,img,high_threshold=0.9,low_threshold=0.75,gamma_adjust=0.6,
                    skeletonize_method="lee"):
        out = img.copy()
        out = exposure.adjust_gamma(out,gamma_adjust)
        out = filters.apply_hysteresis_threshold(out, low_threshold, high_threshold)
        out = morphology.binary_opening(out)
        out = morphology.binary_erosion(out)

        return morphology.skeletonize(out, method=skeletonize_method)

    def overalyDetection(self,img,detection):
        img_ov=img.copy()
        idx=detection==np.max(detection)
        img_ov[idx,0]=255
        img_ov[idx,1]=np.floor(img_ov[idx,1]/2)
        img_ov[idx,2]=np.floor(img_ov[idx,2]/2)
        return img_ov

    def __call__(self, img):
        roi = self.extractROI(img)
        pre = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
        pre = self.enhanceFeatures(pre)
        bin = self.detectRoots(pre)
        ovl = self.overalyDetection(roi,bin)
        return bin, pre, roi, ovl

################################################################################
### FUNC DEF ###################################################################
################################################################################
def computeBackgroundAverage(preprocessor,path_to_imgs,smth_sigma=1):
    """
    returns background avg with values in [0,1] flaot.
    """
    bgnd_avg = None
    n=0;
    images = glob.glob(os.path.join(path_to_imgs,"*.jpg"))
    for fname in images:
        img = cv2.imread(fname)

        try:
            img=preprocessor(img)
        except Exception as e:
            print(f"Skip image {fname} with following exception:\n {e}")
            continue

        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        gray=filters.gaussian(gray,sigma=smth_sigma)
        if bgnd_avg is None:
            bgnd_avg = np.zeros(gray[0].shape)

        bgnd_avg=np.add(bgnd_avg,gray/np.max(gray))
        n=n+1

    if bgnd_avg is not None:
        return bgnd_avg/n

    return bgnd_avg
