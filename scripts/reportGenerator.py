from lxml import etree as ET
import os
import datetime

class HTMLReportGenerator(object):
    """docstring for HTMLReportGenerator."""

    def __init__(self, path_to_report, gui, report_name='sample_data.html'):
        super(HTMLReportGenerator, self).__init__()
        self.path_to_report=path_to_report
        self.report_name=report_name
        self._gui=gui
        #check if report file is around
        try:
            data = open(os.path.join(self.path_to_report,self.report_name),'r').read()
            doc = ET.HTML(data)
        except:
            doc=None

        if doc is None:
            html=ET.Element("html", {"lang":"en", "dir":"ltr"})
            head=html.makeelement("head")
            head.append(head.makeelement("meta", {"charset":"utf-8"}))
            head.append(
                head.makeelement("link", {"rel":"stylesheet", "href":"css/sample_data.css"})
            )
            html.append(head)
            body=html.makeelement("body")
            body.text=""
            html.append(body)

            self._writeHtmlFile(os.path.join(self.path_to_report,self.report_name), html)

    def _reformatMessage(self,str):
        return str.replace("\n","<br>")

    def _openHtmlFile(self, path_to_file):
        try:
            data = open(path_to_file,'r').read()
            doc = ET.HTML(data)
        except:
            doc=None

        return doc

    def _writeHtmlFile(self, path_to_file, html):
        with open(path_to_file, "w") as html_file:
            html_file.write(ET.tostring(html).decode("utf-8").replace("\n","<br>"))

    def _addReportData(self,root,data_str_arr):
        #create sample_data element
        elem = root.makeelement("div", {"class" : "sample_data"})
        #create root_data sub elements
        for str in data_str_arr:
            message=elem.makeelement("div", {"class" : "root_data"})
            message.text=str
            elem.append(message)

        root.insert(0,elem)

    def _requireGUIupdate(self):
        pass#self._gui.report_update()

    def addDataToReport(self,data_str_arr):
        #load HTML file
        doc=self._openHtmlFile(os.path.join(self.path_to_report,self.report_name))
        root=doc[1] #get body

        #create info element
        self._addReportData(root,data_str_arr)

        #save file
        self._writeHtmlFile(os.path.join(self.path_to_report,self.report_name), doc)
        self._requireGUIupdate()

    def convertToTxt(self):
        doc=self._openHtmlFile(os.path.join(self.path_to_report,self.report_name))
        body=doc[1] #get body

        path_to_file=os.path.join(self.path_to_report,self.report_name.replace("html","txt"))

        try:
            os.remove(path_to_file)
        except:
            pass

        with open(path_to_file, "a") as file:
            for el in body:
                for str in el:
                    txt=ET.tostring(str).decode("utf-8").replace("<br/>","\n")
                    txt=txt.replace("<div class=\"root_data\">","").replace("</div>","")
                    txt=txt.replace("&#176;","deg").replace("&gt;",">")

                    file.write(f"\n---> {txt}\n")
