# %% Imports
import serial # import serial library
import imutils
from time import sleep

try:
    from scripts.class_prototypes.robotCtrlBase import *
except:
    from class_prototypes.robotCtrlBase import *

class CartesianRobot(RobotCtrlBase):
    """docstring for CartesianRobot."""

    def __init__(self, serial_port, baudrate, timeout):
        super(CartesianRobot, self).__init__()

        self._serial = serial.Serial(serial_port, baudrate, timeout=timeout)
        sleep(0.5)

        self.home()
        x,y,z=self.readPosition()
        print(f"Homed succesfully at ({x}, {y}, {z})")

        self.turnOnLEDs()
        sleep(0.1)
        self.turnOffLEDs()
        self._led=False

    def __del__(self):
        self._serial.close()

    def _sendCommand(self, command_str):
        self._serial.write(str.encode(command_str))

    def _waitForAck(self, expected_answer = b'ok\n'):

        while True:
            line = self._serial.readline()

            if line == expected_answer:
                break

            sleep(0.01)

    def _decodePositionMsg(self, msg):
        #message loks like this: 'X:{} Y:{} Z:{} E:{} Count A:{} B:{} Z:{}\n'
        temp = msg.decode("utf-8").split(' ')

        if len(temp) != 8:
            return None, None, None

        idx = ["X:","Y:","Z:"]
        coords = [None,None,None]

        for i in range(3):
            try:
                coords[i] = float(temp[i].split(idx[i])[1])
            except:
                #in this case the string is not valid and we do not continue
                coords = [None,None,None]
                break

        return coords[0], coords[1], coords[2]

    """
    The following routines are non-blocking and are queued by the robot.
    """
    def move(self, x, y, z, g_code = 0):
        self._sendCommand(f"G{g_code} X{x} Y{y} Z{z}\r\n")
        self._waitForAck()

    def home(self):
        self._sendCommand("G28\r\n")
        self._waitForAck()

    def moveX(self, x, speed=2400, g_code = 0):
        self._sendCommand(f"G{g_code} X{x} F{speed}\r\n")
        self._waitForAck()

    def moveY(self, y, speed=2400, g_code = 0):
        self._sendCommand(f"G{g_code} Y{y} F{speed}\r\n")
        self._waitForAck()

    def moveXY(self, x, y, speed=2400, g_code = 0):
        self._sendCommand(f"G{g_code} X{x} Y{y} F{speed}\r\n")
        self._waitForAck()

    def moveZ(self, z, g_code = 0):
        self._sendCommand(f"G{g_code} Z{z}\r\n")
        self._waitForAck()

    def turnOnLEDs(self):
        self._sendCommand(f"M140 S50\r\n")
        self._waitForAck()
        self._led=True

    def turnOffLEDs(self):
        self._sendCommand(f"M140 S0\r\n")
        self._waitForAck()
        self._led=False

    def toggleLED(self):
        if self._led:
            self.turnOffLEDs()
        else:
            self.turnOnLEDs()

    def isLEDOn(self):
        return self._led

    def readPosition(self):
        self._sendCommand("M114\r\n")

        while True:
            line = self._serial.readline()

            if line == b'ok\n':
              break

            if line != b'' and line != b'echo:busy: processing\n':
                x,y,z = self._decodePositionMsg(line)

            sleep(0.1)

        return x,y,z
