import cv2
import numpy as np
import glob
import matplotlib.pyplot as plt
from skimage import exposure, filters, morphology, measure
import scipy
import tifffile
try:
    import scripts.util as util
except:
    import util as util

class Link(object):
    """u ready to save Zelda"""

    def __init__(self, previous=None, next=None):
        super(Link, self).__init__()
        self.previous=previous #one parent only
        self.next=[next] #multiple childs

class RootLink(Link):
    def __init__(self, x, y, previous=None, next=None, segment_id=None, avg_intervall=10):
        super(RootLink, self).__init__(previous, next)
        self.x = x
        self.y = y
        self.segment_id = segment_id

        #compute average intervall
        if previous is None:
            self.N=avg_intervall
        else:
            self.N=previous.N

        if previous is None:
            self.x_avg=0
            self.delta_x=0
            self.y_avg=0
            self.delta_y=0
        else:
            #move up the chain to parent N+1 at most
            i=0
            parent=previous
            next_parent=previous.previous
            while i<self.N and next_parent is not None:
                parent=next_parent
                next_parent=parent.previous
                i=i+1


            if next_parent is None:
                #not yet more than N samples in chain, simpliy add the term
                self.x_avg=previous.x_avg+previous.x
                self.y_avg=previous.y_avg+previous.y
            else:
                self.x_avg=previous.x_avg+(previous.x-next_parent.x)
                self.y_avg=previous.y_avg+(previous.y-next_parent.y)

            self.delta_x=self.x-self.x_avg/(i+1)
            self.delta_y=self.y-self.y_avg/(i+1)

        # deviation from the y-axis -> (+) towards the right, (-) towards the left
        self.growth_direction=np.arctan2(self.delta_x,self.delta_y) #rads

class Chain(object):
    """docstring for Chain."""

    def __init__(self,initial_link):
        super(Chain, self).__init__()
        self.first=initial_link
        self.deepest=initial_link
        self.terminations=[]
        self.number_segments=1
        self.link_count=1

    def runAlongChain(self,link=None,do_this=None,*do_args):
        if link is None:
            return

        if do_this is not None:
            do_this(self,link,*do_args)

        for child in link.next:
            if child is None:
                break
            else:
                self.runAlongChain(child,do_this,*do_args)

    def doStuffWhileRunAlongChainPrototype(chain,link,*other_args):
        """
        this function is only a prototype to copy paste when you are
        defining your do_this when calling the runAlongChain method
        """
        raise NotImplementedError

    def appendLinks(self,links):
        """
        Addes one link to the chain making it longer
        """
        # check if first and last link exist
        if self.first is None:
            if len(links)>1:
                raise Exception(f"Too many start links for a chain:\n {links}")
            else:
                self.first=links[0]
                self.deepest=links[0]
            return

        self.deepest.next=links
        for link in links:
            link.previous=self.deepest

        #set the last link of the pased links to be the end link
        self.deepest=link

    def findNewLink(self,parent,chains_img,visited,segment_id=0):
        if parent is None:
            return segment_id

        parent.segment_id=segment_id

        #currently a simple implementation, to upgrade later
        new_links=[]
        ny,nx=chains_img.shape
        #look for adiacent pixels
        # x  x  x
        # x  +  x
        # x  x  x
        x=parent.x-1
        y=parent.y
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x+1
        y=parent.y
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x+1
        y=parent.y+1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x
        y=parent.y+1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x-1
        y=parent.y+1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x-1
        y=parent.y-1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x+1
        y=parent.y-1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True

        x=parent.x
        y=parent.y-1
        if 0<=x<nx and 0<=y<ny and chains_img[y,x] and not visited[y,x]:
            new_links.append(RootLink(x,y,previous=parent))
            visited[y,x]=True


        self.link_count=self.link_count+len(new_links)

        if new_links == []:
            self.terminations.append(parent)
            new_links=[None]

        parent.next=new_links

        for link in new_links:
            if link is not None:
                if self.deepest is None or self.deepest.y<link.y:
                    self.deepest=link

            if len(new_links)>1:
                segment_id=segment_id+1

            segment_id=self.findNewLink(link,chains_img,visited,segment_id)

        return segment_id

    def findChain(self,chains_img,visited):
        self.number_segments=self.findNewLink(self.first,chains_img,visited)+1

    def getElementById(self, id, link=None):
        """
        Only works if the link element possesses an id property
        """
        if link is None:
            return None

        if link.id==id:
            return link

        for child in link.next:
            if child is None:
                found=None
            else:
                found=self.getElementById(id,child)

        return found


class Segment(Link):
    def __init__(self, link, parent_segment):
        super(Segment, self).__init__(previous=parent_segment)
        if parent_segment is not None:
            if parent_segment.next[0] is not None:
                parent_segment.next.append(self)
            else:
                parent_segment.next=[self]

        self.id = link.segment_id
        self.length = 0
        #avg_growth_direction = cumulative_growth_direction/px_count
        self.cumulative_growth_direction = link.growth_direction
        self.px_count = 1
        self.first = link
        self.last = link
        self.is_terminal=False
        self.is_noise=False

    def getAvgGrowthDirection(self):
        return self.cumulative_growth_direction/self.px_count

class Root(Chain):
    """docstring for Root."""

    def __init__(self, initial_link):
        super(Root, self).__init__(initial_link)
        self.ramifications=[]
        self.segments=[]
        self.main_root_path=None
        self.main_root_length=0
        self.total_length=0
        self.root_depth=0
        self.root_graph=None
        self.is_root=1.0
        self.average_growth_direction=0
        self.left_most_x=None
        self.right_most_x=None
        self.root_classification_id=None

    @staticmethod
    def findRootChunks(bin_img):
        roots=[]
        ny,nx=bin_img.shape
        visited=np.zeros((ny,nx),dtype=bool)

        for y in range(ny):
            for x in range(nx):
                if bin_img[y,x] and not visited[y,x]:
                    visited[y,x]=True
                    r=Root(RootLink(x,y,segment_id=0))
                    r.findChain(bin_img,visited)

                    #recompute wrongly detected roots
                    r.recomputeRoot(bin_img,visited)

                    r.extractRootFeatures()
                    r.preclassifyRoot()
                    roots.append(r)

                visited[y,x]=True

        return roots

    @staticmethod
    def findRoots(bin_img):
        roots=Root.findRootChunks(bin_img)

        ##Check if a chunk need to be added to the root
        for root in roots:
            root.preclassifyRoot()
            root.extractRootFeatures()

            if not root.is_root:
                continue

            #1. draw current root to avoid adding itself
            visited=np.zeros(bin_img.shape,dtype=bool)
            def drawRoot(chain,link,img):
                img[link.y,link.x]=True

            root.runAlongChain(root.first,drawRoot,visited)

            #2. While there are terminations to be checked
            termination_check_count=0
            while termination_check_count < len(root.terminations):
                #2.1 Reset counter as new terminations might be added with the
                #    new chunks
                termination_check_count=0

                #2.2 For each termination
                for i in range(len(root.terminations)):
                    t = root.terminations[i]

                    #2.2.1 Load search kernel
                    search_kernel=util.load_kernel_from_png(path_to_data="scripts/data/")
                    search_kernel=util.rotate_kernel(search_kernel,t.growth_direction)
                    w,h=search_kernel.shape
                    delta_w=int(w/2)
                    delta_h=int(h/2)
                    search_kernel[delta_w,delta_h]=False

                    #2.2.2 Extract neighbouthood around termination
                    nbh=util.get_nbh_zero_padding(bin_img, t.y, t.x, w=w, h=h)

                    #2.2.3 Check if there are adiacent chunks
                    y,x=np.where(np.logical_and((nbh>0),search_kernel))
                    x_search=x-delta_w+t.x
                    y_search=y-delta_h+t.y

                    #2.2.4 If any chunks are detected add them
                    if len(x_search)==0:
                        termination_check_count=termination_check_count+1
                        continue
                    else:
                        added_segments=[]

                        #2.2.4.1 For each detected chunk
                        for x,y in zip(x_search,y_search):
                            if visited[y,x] or (x==t.x and y==t.y):
                                continue

                            #2.2.4.1.1 Extract a chain from chunk
                            visited[y,x]=True
                            r=Root(RootLink(x,y,segment_id=0))
                            r.findChain(bin_img,visited)
                            r=r.recomputeRoot(bin_img,visited)
                            r.extractRootFeatures()

                            #2.2.4.1.2 Add chunk if valid
                            chunk_below_termination=True
                            for tt in r.terminations:
                                if tt.y < t.y:
                                    chunk_below_termination=False
                                    break
                            chunk_grows_downwards= np.abs(r.average_growth_direction) < 4.5/9*np.pi

                            if chunk_below_termination and chunk_grows_downwards:
                                #TODO: Add here gap filling through linear interpolation
                                r.first.previous=t

                                if t.next[0] is None:
                                    t.next=[r.first]
                                else:
                                    t.next.append(r.first)

                                added_segments.append(len(r.segments))

                        #2.2.5 If no chunks have been added, go to next
                        if t.next[0] is None:
                            termination_check_count=termination_check_count+1
                            continue

                        #2.2.6 Adapt the segment id for the added segments
                        def recomputeId(chian,link,id_start,current_id):
                            if link.segment_id==0:
                                link.segment_id=current_id
                            else:
                                link.segment_id=link.segment_id+id_start

                        if len(t.next)==1:
                            root.runAlongChain(t.next[0],recomputeId,root.number_segments-1,t.segment_id)
                            root.number_segments=root.number_segments+added_segments[0]-1#THE PROBLEM IS HERE I GUESS
                        else:
                            for n,nb_segments in zip(t.next,added_segments):
                                root.runAlongChain(n,recomputeId,root.number_segments,root.number_segments)
                                root.number_segments=root.number_segments+nb_segments

                        #2.2.7 Update root features
                        root.extractRootFeatures()
                        break

        ## Get rid of clustered duplicates
        def drawRoot(chain,link,img):
            img[link.y,link.x]=True
        i=0
        while i<len(roots)-1:
            popped=False
            for i in range(len(roots)):
                visited=np.zeros(bin_img.shape,dtype=bool)
                root=roots[i]
                root.runAlongChain(root.first,drawRoot,visited)
                for j in range(len(roots)):
                    if j==i:
                        continue
                    else:
                        x=roots[j].first.x
                        y=roots[j].first.y
                        if visited[y,x]:
                            roots.pop(j)
                            popped=True
                            break
                if popped:
                    i=0
                    break

        return roots

    def recomputeRoot(self,bin_img,visited):
        def unvisitRoot(chain,link,visited_map):
            visited_map[link.y,link.x]=False

        new_root=self

        if len(self.first.next)==2 and len(self.terminations)==2:
            self.runAlongChain(self.first,unvisitRoot,visited)
            if self.terminations[0].y<self.terminations[1].y:
                visited[self.terminations[0].y,self.terminations[0].x]=True
                new_root=Root(RootLink(self.terminations[0].x,self.terminations[0].y,segment_id=0))
            else:
                visited[self.terminations[1].y,self.terminations[1].x]=True
                new_root=Root(RootLink(self.terminations[1].x,self.terminations[1].y,segment_id=0))
            new_root.findChain(bin_img,visited)

        return new_root

    def findRamifications(self):
        self.ramifications=[]
        self.terminations=[]
        self.left_most_x=self.first.x
        self.right_most_x=self.first.x
        def findRamificationLinks(chain,link,root):
            if link.x>root.right_most_x:
                root.right_most_x=link.x
            if link.x<root.left_most_x:
                root.left_most_x=link.x

            if len(link.next)>1:
                root.ramifications.append(link)
            elif link.next[0] is None:
                root.terminations.append(link)
                if link.y>root.deepest.y:
                    root.deepest=link

        self.runAlongChain(self.first,findRamificationLinks,self)

    def computeRootSegments(self):
        #container class for result
        class Extractor(object):
            """needed as I don't know how to pass references/pointers"""

            def __init__(self):
                super(Extractor, self).__init__()
                self.segments=[]
        #function to extract result
        def segmentRoot(chain, link, root, extractor):
            if extractor.segments==[]:
                extractor.segments=[None]*root.number_segments
                extractor.segments[link.segment_id]=Segment(link,None)
            elif link.segment_id != link.previous.segment_id:
                extractor.segments[link.segment_id]=Segment(link,extractor.segments[link.previous.segment_id])
            else:
                delta_x=link.x-link.previous.x
                delta_y=link.y-link.previous.y
                dist=np.linalg.norm([delta_x,delta_y])
                extractor.segments[link.segment_id].length=extractor.segments[link.segment_id].length+dist
                extractor.segments[link.segment_id].cumulative_growth_direction=extractor.segments[link.segment_id].cumulative_growth_direction+link.growth_direction
                extractor.segments[link.segment_id].px_count=extractor.segments[link.segment_id].px_count+1
                extractor.segments[link.segment_id].last=link
        #extract root graph
        e=Extractor()
        self.runAlongChain(self.first,segmentRoot,self,e)
        self.root_graph=Chain(e.segments[0])
        #get terminations and total length
        self.total_length=0
        self.average_growth_direction=0
        self.segments=e.segments
        self.number_segments=len(self.segments)

        for segment in e.segments:
            if segment is not None and segment.next[0] is None:
                self.root_graph.terminations.append(segment)
                segment.is_terminal=True
            if segment.px_count<10 and len(e.segments)>1:
                segment.is_noise=True
            else:
                segment.is_noise=False
                self.total_length=segment.length+self.total_length
                self.average_growth_direction=self.average_growth_direction+segment.getAvgGrowthDirection()/len(e.segments)

    def computeMainRoot(self):
        main_root=[]
        main_root_length=0
        for t in self.root_graph.terminations:
            temp_length=0
            temp_path=[]
            while t is not None:
                temp_path.insert(0,t.id)
                temp_length=temp_length+t.length
                t=t.previous

            if temp_length>main_root_length:
                main_root_length=temp_length
                main_root=temp_path

        self.main_root_path=main_root
        self.main_root_length=main_root_length

    def computeRootDepth(self):
        self.root_depth=self.deepest.y-self.first.y

    def computeRootWidth(self):
        pass

    def extractRootFeatures(self):
        self.findRamifications()
        self.computeRootDepth()
        self.computeRootSegments()
        self.computeMainRoot()

    def preclassifyRoot(self,assumptions=[True,True,True]):
        """
        Root classification based on root features alone
        """
        #Assumption 1: 1px roots are not a root
        if assumptions[0] and self.total_length<=1:
            self.is_root=0.
            return
        else:
            self.is_root=1.

        #Assumption 2: a root will not grow against gravity
        #This one needs to be elaborated: if termination looks upward and
        #segment length is considerable we might discard it, otherwise it might
        #be a noise segment due to bad skeletonize
        if assumptions[1]:
            for segment in self.segments:
                grows_upwards=np.abs(segment.last.growth_direction) > 4.5/9*np.pi

                if segment.is_terminal and not segment.is_noise and grows_upwards:
                    self.is_root=0.
                    return

        #Assumption 3: a root starts in the upper part of the ROI
        if assumptions[2] and self.first.y>25:
            self.is_root=0.
            return
        else:
            self.is_root=1.

    @staticmethod
    def computeRootNeighbourhood(root_img,kernel=None):
        if kernel==None:
            def getGaussianKernel(sigma=3):
                size = 6*sigma + 3
                x = np.arange(0, size, 1, float)
                y = x[:, np.newaxis]
                x0, y0 = 3*sigma + 1, 3*sigma + 1
                g = np.exp(- ((x - x0) ** 2 + (y - y0) ** 2) / (2 * sigma ** 2))
                return g

            kernel=getGaussianKernel()


        n,m=kernel.shape
        if not n%2 or m!=n:
            raise Exception(f"kernel ({n} x {m}) for nbhd computation must be squre and side must be odd in px count")
        mid=int(np.floor(n/2))
        print(mid)

        ny,nx=root_img.shape
        nbh=np.zeros(root_img.shape)

        for x in range(nx):
            for y in range(ny):
                if root_img[y,x]:
                    for xx in range(n):
                        for yy in range(n):
                            xxx=x-mid+xx
                            yyy=y-mid+yy

                            if xxx<0 or yyy<0 or xxx>=nx or yyy>= ny:
                                continue

                            nbh[yyy,xxx]=np.maximum(nbh[yyy,xxx],kernel[yy,xx])

        return nbh

    def printData(self,sample_id,root_idx,logger=None, verbose=False):
        def printRootGraph(chain,link,extractor):
            if link.previous is None:
                extractor.str=f"{extractor.str}(0), growth_direction: {(link.getAvgGrowthDirection() * 180 / np.pi):.2f}°\n"
            for n in link.next:
                if n is not None:
                    extractor.str=f"{extractor.str}({link.id})--->({n.id}), growth_direction: {(n.getAvgGrowthDirection() * 180 / np.pi):.2f}°\n"
        class Extractor(object):
            def __init__(self,str):
                super(Extractor, self).__init__()
                self.str=str

        str="--- ROOT DATA ----------\n"
        str=f"{str}For Root {root_idx} of sample {sample_id}\n"
        str=f"{str}Starting Coordinates: ({self.first.x},{self.first.y})\n"
        str=f"{str}Depth: {self.root_depth:.2f}\n"
        str=f"{str}Width: {self.right_most_x-self.left_most_x}\n"
        str=f"{str}Main Root: {self.main_root_path}, {self.main_root_length:.2f}\n"
        str=f"{str}Number of Ramifications: {(len(self.segments)-len(self.main_root_path))}\n"
        str=f"{str}Total Length: {self.total_length:.2f}\n"
        str=f"{str}Average Growth Direction: {(self.average_growth_direction * 180 / np.pi):.2f}°\n"
        e=Extractor(str)
        self.root_graph.runAlongChain(self.root_graph.first,printRootGraph,e)
        str=e.str
        str=f"{str}------------------------\n"

        if logger is None and verbose:
            print(str)
        elif verbose:
            logger.info(str)

        return str


    def generateRootKernel(self):
        w=int(2*(self.right_most_x-self.left_most_x+12)+1)
        h=int(2*(self.root_depth+12)+1)
        kernel=np.zeros((h,w))

        def drawRoot(chain,link,kernel,mid_x,mid_y):
            x=link.x-chain.first.x+mid_x
            y=link.y-chain.first.y+mid_y

            kernel[y,x]=1.0

        self.runAlongChain(self.first,drawRoot,kernel,int(w/2),int(h/2))

        kernel=kernel/np.sum(kernel)

        return kernel
