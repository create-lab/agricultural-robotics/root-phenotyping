import pickle
import time

try:
    from scripts.class_prototypes.sampleTrackerBase import *
except:
    from class_prototypes.sampleTrackerBase import *

class Sample(object):
    """docstring for Sample."""

    def __init__(self, id,home_slot=None,recording_time_intervall_s=0,plant_count=0):
        super(Sample, self).__init__()
        self.id=id
        self.home_slot=home_slot
        self.img_count=0
        self.plant_count=int(plant_count)
        # for scheduling purposes
        self.last_theoretical_recording_time=time.time()
        self.last_recording_time=time.time()
        self.time_between_recordings_s=recording_time_intervall_s
        self.has_to_be_recorded=False
        self.next_recording_time=time.time()
        self.time_overdue=0
        # other data
        self.last_picture=None
        self.last_extracted_data=None

    def updateImgCount(self,img_name):
        self.img_count=self.img_count+1
        self.last_picture=img_name

    def updateRecordingTime(self):
        self.has_to_be_recorded=False
        self.last_recording_time=time.time()
        self.next_recording_time=self.last_theoretical_recording_time+self.time_between_recordings_s
        self.time_overdue=self.last_recording_time-self.last_theoretical_recording_time

    def scheduleRecordingTask(self):
        currrent_t=time.time()

        if self.has_to_be_recorded:
            #this avoids generating multiple tasks for the same recording
            return False
        elif currrent_t>=self.next_recording_time:
            self.has_to_be_recorded=True
            self.last_theoretical_recording_time=self.next_recording_time

        return self.has_to_be_recorded

class SampleTracker(SampleTrackerBase):
    """docstring for SampleTracker."""

    def __init__(self, number_of_slots,logger=None):
        super(SampleTracker, self).__init__()

        self._rack = [None] * number_of_slots
        self._gripper = None
        self._number_of_slots = number_of_slots
        self._logger=logger

    def _validTarget(self, target_slot):
        return target_slot is not None and 0 <= target_slot < self._number_of_slots

    def getSlotFromId(self,sample_id):
        slot=next(
            (
                i for i, sample in enumerate(self._rack) if \
                sample is not None and sample.id == sample_id
            ),
            None
        )
        if slot is None:
            str=f"No object with required id={sample_id} found"
            print(str)
            if self._logger is not None: self._logger.error(str)

        return slot

    def getSampleHome(self,sample_id):
        if self._gripper is not None and self._gripper.id == sample_id:
            return self._gripper.home_slot
        else:
            return self.getSlotFromId(sample_id)

    def getSamples(self):
        samples = []
        if self._gripper is not None:
            samples.append(self._gripper)
        for sample in self._rack:
            if sample is not None:
                samples.append(sample)
        return samples

    def getSampleById(self,smaple_id):
        for sample in self._rack:
            if sample.id == sample_id:
                return sample

        if self._gripper is not None and self._gripper.id == sample_id:
            return self._gripper


    def getTotalSlots(self):
        return [self._gripper, *self._rack]

    def saveData(self):
        try:
            with open("data/rack_data.pickle", "wb") as f:
                pickle.dump(self._rack, f, protocol=pickle.HIGHEST_PROTOCOL)
            with open("data/gripper_data.pickle", "wb") as f:
                pickle.dump(self._gripper, f, protocol=pickle.HIGHEST_PROTOCOL)
        except Exception as ex:
            print("Unable to save data with following exception:\n", ex)

    def loadData(self,filename_rack="data/rack_data.pickle",
                 filename_gripper="data/gripper_data.pickle"):
        try:
            with open(filename_rack, "rb") as f:
                self._rack = pickle.load(f)

                diff=self._number_of_slots-len(self._rack)
                if diff>0:
                    for _ in range(diff):
                        self._rack.append(None)

                    for i in range(len(self._rack)):
                        if self._rack[i] is not None and self._rack[i].has_to_be_recorded:
                            #set flag to false to recreate task if system shutdown occurs
                            self._rack[i].has_to_be_recorded=False
                elif diff<0:
                    raise Exception(
                        f"Too many samples to load for given hardware, have {len(self._rack)}, "
                        f"should have {self._number_of_slots}"
                    )

            with open(filename_gripper, "rb") as f:
                self._gripper = pickle.load(f)
        except Exception as ex:
            self._rack = [None] * self._number_of_slots
            self._gripper = None
            print("Unable to load data with following exception:\n", ex)

    def addSample(self, sample, target_slot, sample_overwrite=False):
        if self.slotIsLoaded(target_slot) and not sample_overwrite:
            str=f"Slot {target_slot} is already occupied"
            print(str)
            if self._logger is not None: self._logger.warn(str)
            return False

        sample.home_slot = target_slot
        self._rack[target_slot] = sample

        self.saveData()
        return True

    def removeSample(self,sample_id):
        target_slot=self.getSlotFromId(sample_id)
        self._rack[target_slot] = None

    def initializeSlots(self,total_slots):
        self._rack = total_slots
        self._gripper = None

    def slotIsLoaded(self, target_slot):
        return self._rack[target_slot] is not None

    def holdingSample(self):
        return self._gripper is not None

    def checkPickup(self, sample_id, warn=True):
        slot=self.getSlotFromId(sample_id)

        if self._validTarget(slot):
            holding = self.holdingSample()

            if self.slotIsLoaded(slot) and not holding:
                return True
            if holding and warn:
                str=f"Holding sample {self._gripper.id}, can't pickup at target slot {slot}"
                if self._logger is not None: self._logger.warn(str)
                print(str)
            elif warn:
                str=f"Target slot {slot} is empty, can't pick up anything"
                if self._logger is not None: self._logger.warn(str)
                print(str)
        elif warn:
            str=f"target slot {slot} is invalid"
            if self._logger is not None: self._logger.warn(str)
            print(str)

        return False

    def trackPickip(self, sample_id, warn=False):
        if self.checkPickup(sample_id, warn):
            slot=self.getSlotFromId(sample_id)
            self._gripper = self._rack[slot]
            self._rack[slot] = None
            self.saveData()
            return True

        self.saveData()
        return False

    def checkPut(self, slot, position_overwrite=False, warn=True):
        if self._validTarget(slot):
            holding = self.holdingSample()

            if holding and not self.slotIsLoaded(slot):
                mismatch = slot != self._gripper.home_slot

                if mismatch and not position_overwrite:
                    if warn:
                        str=(
                            f"Unauthorized attempt to put sample {self._gripper.id} into slot "
                            f"{slot}, abort put"
                        )
                        if self._logger is not None: self._logger.warn(str)
                        print(str)
                    return False

                if mismatch and warn:
                    str=f"Position Overwrtie: Sample {self._gripper.id} will be put in slot {slot}"
                    if self._logger is not None: self._logger.warn(str)
                    print(str)

                return True

            if not holding and warn:
                str=f"Not holding any sample, can't put anything"
                if self._logger is not None: self._logger.warn(str)
                print(str)
            elif warn:
                str=f"Slot {slot} is occupied, can't put current sample {self._gripper.id}"
                if self._logger is not None: self._logger.warn(str)
                print(str)
            return False
        else:
            return False

    def trackPut(self, slot, position_overwrite=False, warn=False):
        if self.checkPut(slot,position_overwrite,warn):
            self._rack[slot] = self._gripper
            self._gripper = None
            #update home slot to assure position overwrite tracking
            self._rack[slot].home_slot = slot

            self.saveData()
            return True

        self.saveData()
        return False

    def getHoldingSample(self):
        return self._gripper
