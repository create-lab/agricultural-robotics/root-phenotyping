import numpy as np
import time
import datetime

class Task(object):
    #objective definition:
    RECORD_SAMPLE   = 0
    MOVE_SAMPE      = 1
    LIGHTS_ON       = 2
    LIGHTS_OFF      = 3
    PICKUP_SAMPLE   = 4
    PUT_SAMPLE      = 5
    CV_TASK         = 6
    #priorites
    HIGH_PRIORITY   = 0
    NORMAL_PRIORITY = 1
    LOW_PRIORITY    = 2
    def __init__(self, objective, priority=2, sample_id=None, from_slot=None,
                 to_slot=None):
        super(Task, self).__init__()
        self.is_processing = False
        self.is_done = False
        self.objective = objective
        self.sample_id=sample_id
        self.from_slot=from_slot
        self.to_slot=to_slot
        self.priority=int(np.minimum(Task.LOW_PRIORITY,np.maximum(Task.HIGH_PRIORITY,priority)))
        self.time_stamp=datetime.datetime.now()

    def taskStr(self):
        obj="Unknown Task"

        if self.objective == Task.RECORD_SAMPLE:
            obj="Record Sample"
        elif self.objective == Task.MOVE_SAMPE:
            obj="Move Sample"
        elif self.objective == Task.LIGHTS_ON:
            obj="Turn On Lights"
        elif self.objective == Task.LIGHTS_OFF:
            obj="Turn Off Lights"
        elif self.objective == Task.PICKUP_SAMPLE:
            obj="Pickup Sample"
        elif self.objective == Task.PUT_SAMPLE:
            obj="Put Sample"

        return (
            f"Task with objective {obj} for:\n - sample_id {self.sample_id} \n - from_slot "
            f"{self.from_slot}\n - to_slot {self.to_slot}\n"
        )

class SimpleDate(object):
    """struct more than class but still, in my head it made sense... lol"""

    def __init__(self, hh,mm):
        super(SimpleDate, self).__init__()
        self.hh=hh
        self.mm=mm

    def setTime(self,hh,mm):
        self.hh=hh
        self.mm=mm

class Scheduler(object):
    """
    This class holds the necessary stuff to creat a scheduling thread, which can
    be launched with the run() method. Scheduling is done with FIFO logic.
    Schedules:
     - auto recordings
     - rotation of samples
     - day/night cycles
     - allows to introduce manually tasks in work queue, with schedulet time
       asap
     3 priorities:
      - high: for auto recording and day night cycle
      - normal: for user set tasks
      - low: rotation of samples
    """

    def __init__(self, centrel_ctrl, sample_tracker,logger,day_start=SimpleDate(8,00),
                 night_start=SimpleDate(20,00),rotation_intervall=SimpleDate(23,59),
                 auto_cv_start=False):
        super(Scheduler, self).__init__()
        self.ctrl = centrel_ctrl
        self.tracker = sample_tracker
        self.logger=logger
        self.work_queue = [[],[],[]] #high, medium, low priority
        self.processing_task=None
        self.day_start=day_start
        self.night_start=night_start
        self.rotation_intervall=rotation_intervall
        self.last_rotation_time=datetime.datetime.now()
        self.next_rotation_time=self.last_rotation_time+datetime.timedelta(
            hours=rotation_intervall.hh, minutes=rotation_intervall.mm
        )
        self.pause_task_execution=False
        self.homing_required=False
        self.stop_scheduler=False
        self.auto_cv_start=auto_cv_start

    def stopThread(self):
        self.stop_scheduler=True

    def setNightTimeStart(self,hh,mm):
        self.night_start.setTime(hh,mm)

    def setDayTimeStart(self,hh,mm):
        self.day_start.setTime(hh,mm)

    def setRotationTimeIntervall(self,hh,mm):
        self.rotation_intervall.setTime(hh,mm)

        self.next_rotation_time=self.last_rotation_time+datetime.timedelta(hours=hh, minutes=mm)

    def requireHoming(self):
        self.pause_task_execution=True
        self.homing_required=True

    def pauseTaskExecution(self):
        self.pause_task_execution=True

    def resumeTaskExecution(self):
        self.pause_task_execution=False

    def addTask(self,task):
        str=f"Register new {task.taskStr()}"
        if self.logger is not None: self.logger.info(str)
        print(str)
        if task.objective == Task.LIGHTS_ON or task.objective == Task.LIGHTS_OFF:
            self.work_queue[task.priority].insert(0,task)
        else:
            self.work_queue[task.priority].append(task)

    def scheduleAutoRecording(self):
        #check if samples need to be recorded
        samples=self.tracker.getSamples()

        for sample in samples:
            if sample.scheduleRecordingTask():
                new_task=Task(Task.RECORD_SAMPLE,sample_id=sample.id,priority=Task.HIGH_PRIORITY)
                str=f"Register new {new_task.taskStr()}"
                if self.logger is not None: self.logger.info(str)
                print(str)
                self.work_queue[new_task.priority].append(new_task)

    def scheduleDayNighCycle(self):
        #check if the day or night cycle needs to be started
        now=datetime.datetime.now()
        day_cycle_start=now.replace(hour=self.day_start.hh, minute=self.day_start.mm)
        night_cycle_start=now.replace(hour=self.night_start.hh, minute=self.night_start.mm)

        #handle case for reverse night-day cycles
        schedule_lights_on=now > day_cycle_start
        schedule_lights_off=now > night_cycle_start
        day_comes_first=night_cycle_start > day_cycle_start

        lights_on=(day_comes_first and schedule_lights_on and not schedule_lights_off) or \
                  (not day_comes_first and schedule_lights_on) or \
                  (not day_comes_first and not schedule_lights_on and not schedule_lights_off)

        if lights_on and not self.ctrl.inDayCycle:
            new_task=Task(Task.LIGHTS_ON,priority=Task.HIGH_PRIORITY)
            str=f"Register new {new_task.taskStr()}"
            if self.logger is not None: self.logger.info(str)
            print(str)
            #short task, can be executed immediately
            self.work_queue[new_task.priority].insert(0,new_task)

        if not lights_on and self.ctrl.inDayCycle:
            new_task=Task(Task.LIGHTS_OFF,priority=Task.HIGH_PRIORITY)
            str=f"Register new {new_task.taskStr()}"
            if self.logger is not None: self.logger.info(str)
            print(str)
            #short task, can be executed immediately
            self.work_queue[new_task.priority].insert(0,new_task)

    def scheduleSampleRotation(self):
        slots=self.tracker.getTotalSlots()

        gripper = slots.pop(0)
        if gripper is not None:
            return

        if datetime.datetime.now()>self.next_rotation_time:
            self.last_rotation_time=self.next_rotation_time
            self.next_rotation_time=self.next_rotation_time+datetime.timedelta(
                hours=self.rotation_intervall.hh, minutes=self.rotation_intervall.mm
            )
        else:
            return

        #seek for first empty slot
        rotation_start=0
        n_slots=len(slots)
        while slots[rotation_start] is not None  and rotation_start<n_slots:
            rotation_start=rotation_start+1

        if rotation_start==n_slots:
            #no empty slot found
            return

        for i in range(n_slots-1):
            slot_from=rotation_start-(i+1)

            if slots[slot_from] is not None:
                sample_id=slots[slot_from].id
                slot_to=rotation_start-i
                if slot_to<0:
                    slot_to=n_slots+slot_to
                if slot_from<0:
                    slot_from=n_slots+slot_from
                new_task=Task(
                    Task.MOVE_SAMPE,
                    sample_id=sample_id,
                    from_slot=slot_from,
                    to_slot=slot_to,
                    priority=Task.LOW_PRIORITY
                )
                str=f"Register new {new_task.taskStr()}"
                if self.logger is not None: self.logger.info(str)
                print(str)
                self.work_queue[new_task.priority].append(new_task)


    def updateWorkQueue(self):
        self.scheduleAutoRecording()
        self.scheduleDayNighCycle()
        self.scheduleSampleRotation()

    def handleWorkQueue(self):
        #exctract next task
        if self.processing_task is None:
            if len(self.work_queue[Task.HIGH_PRIORITY])>0:
                self.processing_task=self.work_queue[Task.HIGH_PRIORITY].pop(0)
            elif len(self.work_queue[Task.NORMAL_PRIORITY])>0:
                self.processing_task=self.work_queue[Task.NORMAL_PRIORITY].pop(0)
            elif len(self.work_queue[Task.LOW_PRIORITY])>0:
                self.processing_task=self.work_queue[Task.LOW_PRIORITY].pop(0)

            if self.processing_task is None:
                return

            self.processing_task.is_processing=True

        if self.processing_task.objective == Task.RECORD_SAMPLE:
            self.processing_task.is_done=self.ctrl.recordSample(self.processing_task.sample_id)
            if self.processing_task.is_done and self.auto_cv_start:
                self.addTask(Task(Task.CV_TASK,sample_id=sample.id,priority=Task.NORMAL_PRIORITY))
        elif self.processing_task.objective == Task.MOVE_SAMPE:
            self.processing_task.is_done=self.ctrl.moveSample(
                self.processing_task.sample_id,self.processing_task.to_slot
            )
        elif self.processing_task.objective == Task.LIGHTS_ON:
            self.processing_task.is_done=self.ctrl.startDayCycle()
        elif self.processing_task.objective == Task.LIGHTS_OFF:
            self.processing_task.is_done=self.ctrl.endDayCycle()
        elif self.processing_task.objective == Task.PICKUP_SAMPLE:
            self.processing_task.is_done=self.ctrl.pickupSample(self.processing_task.sample_id)
        elif self.processing_task.objective == Task.PUT_SAMPLE:
            self.processing_task.is_done=self.ctrl.putSample(
                self.processing_task.to_slot,position_overwrite=True
            )
        elif self.processing_task.objective == Task.CV_TASK:
            #start CV thread TODOOO!!!
            self.processing_task.is_done=True

        if not self.processing_task.is_done:
            #there is probably a better way to handle this instead of just discarding it... lol
            str=f"{self.processing_task.taskStr()}\nnot completed succesfully, discarded"
            if self.logger is not None: self.logger.error(str)
            print(str)

        self.processing_task=None


    def run(self):
        while True:
            if self.stop_scheduler:
                return

            self.updateWorkQueue()

            if not self.pause_task_execution:
                self.handleWorkQueue()

            if self.homing_required:
                self.ctrl.homeRobot()
                self.homing_required=False

            time.sleep(1)
