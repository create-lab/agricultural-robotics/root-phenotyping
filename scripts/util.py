"""
Some util functions
"""

import cv2
import os
import numpy as np

def mirrored_periodisation(idx,N):
    ''' mirrored periodisation of signal of length N evaluated at index current_idx
        For a signal s={...0,0,|0|,1,2,3,0,0,...} the result is
        s_p={...2,1,|0|,1,2,3,2,1,0,1,...}
        \param N        support of signal, in example above N=4
        \param idx      index where to evaluate periodic version s_p ot s
    '''
    if idx<0:
        idx=-idx
    elif idx>=N:
        #try and write it out if you want to understand...
        #...it is just a mathematical expression for a mirrored periodic sequence of indices
        idx=(int(idx/(N-1))%2)*(N-1-idx%(N-1))+(1-int(idx/(N-1))%2)*(idx%(N-1))

    #recursive correction
    if idx<0 or idx>=N:
        idx=mirrored_periodisation(idx,N);

    #retunr equivalent index in bounds [0;N-1]
    return idx

def get_nbh_mirrored(img,x,y,w=3,h=3):
    ''' get neighbourhood around x,y pixel with mirrored periodisation, works
        only with odd w,h! no test if odd or even, so be careful!
        \param w,h      width of nbh, height of nbh. Must be odd
        \param x,y      position where nbh is extracted
    '''
    nx,ny=img.shape
    delta_w=int(w/2)
    delta_h=int(h/2)

    nbh=np.zeros((w,h))
    for xx in range(x-delta_w,x+delta_w+1):
        for yy in range(y-delta_h,y+delta_h+1):
            #get equivalent idx in case of values out of bounds
            x_idx=mirrored_periodisation(xx,nx)
            y_idx=mirrored_periodisation(yy,ny)
            #put values in nbh, origin (x,y) in the middle of nbh
            nbh[xx-x+delta_w][yy-y+delta_h]=img[x_idx][y_idx]

    return nbh

def load_kernel_from_png(path_to_data="data/"):
    img = cv2.imread(os.path.join(path_to_data,"kernels/rootSearch.png"))
    kernel=img[:,:,0]/255
    return kernel

def rotate_kernel(kernel,angle):
    center = tuple(np.array(kernel.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(center, angle, 1.0)
    out = cv2.warpAffine(kernel, rot_mat, kernel.shape[1::-1], flags=cv2.INTER_LINEAR)
    #return T/F kernel
    return out > 0

def get_nbh_zero_padding(img,x,y,w=3,h=3):
    ''' get neighbourhood around x,y pixel with zero padding, works
        only with odd w,h! no test if odd or even, so be careful!
        \param w,h      width of nbh, height of nbh. Must be odd
        \param x,y      position where nbh is extracted
    '''
    nx,ny=img.shape
    delta_w=int(w/2)
    delta_h=int(h/2)

    nbh=np.zeros((w,h))
    for xx in range(x-delta_w,x+delta_w+1):
        for yy in range(y-delta_h,y+delta_h+1):
            if xx<0 or yy<0 or xx>=nx or yy>=ny:
                continue
            #put values in nbh, origin (x,y) in the middle of nbh
            nbh[xx-x+delta_w][yy-y+delta_h]=img[xx][yy]

    return nbh

def minmax(x,mi=None,ma=None):
    if mi is None:
        mi=np.min(x)
    if ma is None:
        ma=np.max(x)

    return (x-mi)/(ma-mi)

def saveToMultipageTiff(file_name,data,overwrite=False):
    try:
        if overwrite:
            updated_data=np.array([data])
        else:
            #try loading data if exsistent
            previous_data=tifffile.imread(file_name)
            updated_data=np.append(previous_data,np.array([data]),axis=0)
    except:
        updated_data=np.array([data])

    tifffile.imwrite(file_name,updated_data)
