#include <Wire.h>
#include <Servo.h>
#include <string.h>
#include "pyCommsLib.h"
#include "util.h"

// Setup constants
#define SERVO_PIN 9           //location of signal pin for the servo control
#define LED_PIN 3             //location of pin controlling the led relay
#define BAUDRATE 9600         //baudrate for serial comm between python and arduino
#define SET_MSG_IDX 4         //index of the message payload, header looks like XXX:
#define REVERT_ROT true       //reverts rotation direction
#define MOTION_TO 1500        //motion timout
// TO CALIBRATE
#define MIN_A_POS 13.392f     //CALIBRATED angular position of servo for an open gripper
#define MAX_A_POS 180.f       //CALIBRATED angular position of servo for a close gripper
#define MAX_APERTURE 25.f     //CALIBRATED maximum aperture in mm for gripper
#define MIN_APERTURE 1.86f    //CALIBRATED minimum --------------------------
// Msg enum
enum class GripperStatus {
  NO_ERROR = 0,
  ERROR,
  UNKNOWN_CMD,
  BUSY
};

// Servo instantiation
Servo myservo;

// Func def
bool set_servo_position(float pos_degrees);
void handle_command(String cmd);
float extract_command_value(String cmd);
float convert_aperture_to_angle(float aperture);
bool unknown_command(String cmd);

////////////////////////////////////////////////////////////////////////////////
/// SETUP FUNCTION /////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void setup() {
  // Open serial port at given baudrate
  Serial.begin(BAUDRATE);

  init_python_communication();

  // Attach servo at given pin
  myservo.attach(SERVO_PIN);

  // Set LED pin as output
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
}

////////////////////////////////////////////////////////////////////////////////
/// LOOP FUNCTION //////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void loop() {
  String cmd = latest_received_msg();

  handle_command(cmd);

  sync();
}

////////////////////////////////////////////////////////////////////////////////
/// FUNCTION DEF ///////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool set_servo_position(float pos_degrees) {
  if(pos_degrees >= 0.f && pos_degrees <= 180.f && !isnan(pos_degrees)) {
    // write required position to servo
    myservo.write(pos_degrees);
    //delay(15);

    return true;
  }

  return false;
}

void handle_command(String cmd) {
  float payload, alpha;
  static bool timeout_required = false,  cmd_success=false;
  static float last_position_update = millis()-MOTION_TO;
  static String last_cmd = "";
  bool new_command = last_cmd != cmd;

  if(unknown_command(cmd)){
    set_new_message_payload(String((int)GripperStatus::UNKNOWN_CMD));
    return;
  } else if(cmd == "NO_PYTHON_MESSAGE") {
    //do nothing
    set_new_message_payload(String(""));
    return;
  }

  if(cmd.startsWith("APR:")) {
    if(!timeout_required && new_command) {
      // only APR: command is handled
      payload = extract_command_value(cmd);
      alpha = convert_aperture_to_angle(payload);
      cmd_success = set_servo_position(alpha);
      last_position_update = millis();
      timeout_required=true;
    } else if(millis()-last_position_update >= MOTION_TO) {
      timeout_required = false;
    }
  } else if (cmd.startsWith("LED:")) {
    payload = extract_command_value(cmd);
    if(!timeout_required && new_command && !isnan(payload) && payload > 0.5) {
      digitalWrite(LED_PIN,HIGH);
      cmd_success=digitalRead(LED_PIN)==HIGH;
      timeout_required = false;
    } else if (!timeout_required && new_command && !isnan(payload)) {
      digitalWrite(LED_PIN,LOW);
      cmd_success=digitalRead(LED_PIN)==LOW;
      timeout_required = false;
    } else {
      cmd_success=false;
    }
  }

  last_cmd = cmd;

  if(timeout_required) {
    set_new_message_payload(String((int)GripperStatus::BUSY));
  } else if (cmd_success) {
    set_new_message_payload(String((int)GripperStatus::NO_ERROR));
  } else {
    set_new_message_payload(String((int)GripperStatus::ERROR));
  }
}

float extract_command_value(String cmd) {
  if(cmd=="NO_PYTHON_MESSAGE" || unknown_command(cmd)) {
    return MIN_APERTURE;
  } else {
    //command is header looks like: XXX:
    return (cmd.substring(SET_MSG_IDX)).toFloat();
  }
}

float convert_aperture_to_angle(float aperture) {
  aperture = constrain(aperture, MIN_APERTURE, MAX_APERTURE);
  aperture = minmax(aperture, MIN_APERTURE, MAX_APERTURE);
  aperture = REVERT_ROT ? 1.f - aperture : aperture;
  return (MAX_A_POS-MIN_A_POS)*aperture + MIN_A_POS;
}

bool unknown_command(String cmd) {
  return !cmd.startsWith("APR:") && !cmd.startsWith("LED:") &&
         cmd != "NO_PYTHON_MESSAGE";
}
