#include "util.h"

float minmax(float x, float min_v, float max_v) {
  return (x-min_v)/(max_v-min_v);
}
